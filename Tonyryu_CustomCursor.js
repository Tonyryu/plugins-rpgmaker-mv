﻿﻿//=============================================================================
// Tonyryu_CustomCursor.js
//=============================================================================

/*:
 * @plugindesc Plugin permettant de changer le curseur sur le jeu
 * @author Tonyryu
 *
 * @param CursorImage
 * @desc Url de l'image pour le cursor
 * @default img/pictures/pointer.png
 *
 * @help http://www.tonyryudev.com/
 * 
 */

(function() {
  var parameters = PluginManager.parameters('Tonyryu_CustomCursor');
  var param_cursorImage = String(parameters['CursorImage'] || 'img/picures/pointer.png');

  var Tonyryu_Graphics_createUpperCanvas = Graphics._createUpperCanvas;
  Graphics._createUpperCanvas = function() {
      Tonyryu_Graphics_createUpperCanvas.call(this);
      this._upperCanvas.style.cursor = 'url("'+param_cursorImage+'"), default';
  };
  
  var Tonyryu_Graphics_createErrorPrinter = Graphics._createErrorPrinter;
  Graphics._createErrorPrinter = function() {
      Tonyryu_Graphics_createErrorPrinter.call(this);
      this._errorPrinter.style.cursor = 'url("'+param_cursorImage+'"), default';
  };
})();
