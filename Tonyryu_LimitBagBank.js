//=============================================================================
// Tonyryu_LimitBagBank.js
//=============================================================================

/*:
 * @plugindesc v0.0.9 - Plugin permettant de gérer une limite de l'inventaire
 * @version 0.0.9
 * @author Tonyryu (http://www.tonyryudev.com)
 *
 * @param GroupAllItemType
 * @desc Regrouper tous les types d'objet dans le menu objet (1 : actif)
 * @default 0
 * 
 * @param AddDeleteMenu
 * @desc Ajouter un menu pour proposer le choix de jeter un objet (1 : actif)
 * @default 1
 * 
 * @param NameUseOption
 * @desc Nom de l'option d'utilisation
 * @default Utiliser
 * 
 * @param NameDeleteOption
 * @desc Nom de l'option de suppression
 * @default Jeter
 *
 * @param NbrOfDiffItem
 * @desc Nombre de type d'objet différent au départ
 * @default 20
 *
 * @param ItemStackDefault
 * @desc Quantité par défaut d'un stack pour les objets
 * @default 99
 * 
 * @param ArmorStackDefault
 * @desc Quantité par défaut d'un stack pour les armures
 * @default 1
 * 
 * @param WeaponStackDefault
 * @desc Quantité par défaut d'un stack pour les armes
 * @default 1
 * 
 * @param BankNbrOfDiffItem
 * @desc Nombre de type d'objet différent dans la banque au départ
 * @default 100
 * 
 * @param NameBag
 * @desc Nom de l'utilitaire de stockage du groupe
 * @default Sac
 * 
 * @param NameBank
 * @desc Nom de l'utilitaire de stockage de masse
 * @default Banque
 * 
 * @param NameGetOption
 * @desc Nom de l'option indiquant la récupération d'un objet en banque
 * @default Retirer
 * 
 * @param NamePutOption
 * @desc Nom de l'option indiquant le dépot d'un objet en banque
 * @default Déposer
 * 
 * 
 * @help 
 * 
 */

/*
 * Suivi de version 
 * 
 * 
 */
var Imported = Imported || {};
Imported.Tonyryu_LimitBagBank = true;

var Tonyryu = Tonyryu || {};
Tonyryu.LBB = Tonyryu.LBB || {};

(function() {

  var parameters = PluginManager.parameters('Tonyryu_LimitBagBank');
  var paramGroupAllItemType = Number(parameters['GroupAllItemType'] || 0);
  var paramAddMenuDelete = Number(parameters['AddDeleteMenu'] || 1);
  var paramNameDeleteOption = String(parameters['NameDeleteOption'] || 'Jeter');
  var paramNameUseOption = String(parameters['NameUseOption'] || 'Utiliser');
  var paramNbrOfDiffItem = Number(parameters['NbrOfDiffItem'] || 20);
  var paramItemStackDefault = Number(parameters['ItemStackDefault'] || 99);
  var paramArmorStackDefault = Number(parameters['ArmorStackDefault'] || 1);
  var paramWeaponStackDefault = Number(parameters['WeaponStackDefault'] || 1);
  var paramBankNbrOfDiffItem = Number(parameters['BankNbrOfDiffItem'] || 100);
  var paramNameBank = String(parameters['NameBank'] || 'Banque');
  var paramNameBag = String(parameters['NameBag'] || 'Sac');
  var paramNameGetOption = String(parameters['NameGetOption'] || 'Retirer');
  var paramNamePutOption = String(parameters['NamePutOption'] || 'Déposer');
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Actor
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : unequipPossible (ajout)
  * Fonction : vérifie la possibilité de déséquiper un slot
  * Params : slotId : index du slot
  **********************************************************************/
  Game_Actor.prototype.unequipPossible = function(slotId) {
    var itemEquip = this.equips()[slotId];
    if($gameParty.bagFull() && itemEquip){
      var stackDif = $gameParty.numItemStack(itemEquip) - $gameParty.numItemStack(itemEquip, 1);
      if(stackDif < 0)
        return false;
    }
    return true;
  };

  /**********************************************************************
  * Méthode : changeEquipPossible (ajout)
  * Fonction : vérifie la possibilité de changer d'équipement
  * Params : slotId : index du slot
  *          item   : objet a équipper
  **********************************************************************/
  Game_Actor.prototype.changeEquipPossible = function(slotId, item) {
    var itemEquip = this.equips()[slotId];
    var stackDif = 0;
    if(itemEquip)
      stackDif += $gameParty.numItemStack(itemEquip, 1) - $gameParty.numItemStack(itemEquip);
    if(item)
      stackDif += $gameParty.numItemStack(item, -1) - $gameParty.numItemStack(item);
    return ($gameParty.numAllStack() + stackDif <= $gameParty._bagLimit);
  };
  
  /**********************************************************************
  * Méthode : changeEquip (surcharge)
  * Fonction : vérifie la possibilité de changer un slot
  * Params : slotId : index du slot
  *          item   : objet a équipper
  **********************************************************************/
  var _Game_Actor_changeEquip = Game_Actor.prototype.changeEquip;
  Game_Actor.prototype.changeEquip = function(slotId, item) {
    if(this.changeEquipPossible(slotId, item))
      _Game_Actor_changeEquip.call(this, slotId, item);
  };


  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Party
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : numAllStack (ajout)
  * Fonction : retourne le nombre de stack dans l'inventaire
  * Params : --
  **********************************************************************/
  Game_Party.prototype.numAllStack = function() {
    // pour chaque objet possédés
    var n = 0;
    $gameParty.allItems().forEach(function(item) {
      n += $gameParty.numItemStack(item);
    });
    return n;
  };
  
  /**********************************************************************
  * Méthode : bagFull (ajout)
  * Fonction : permet se savoir si le sac est plein
  * Params : --
  **********************************************************************/
  Game_Party.prototype.bagFull = function() {
    return (this.numAllStack() >= this._bagLimit );
  };
  
  /**********************************************************************
  * Méthode : stackLimit (ajout)
  * Fonction : retourne la limite d'un stack pour un objet
  * Params : --
  **********************************************************************/
  Game_Party.prototype.stackLimit = function(item) {
    var limit = 0 ;
    var typeItem = '';
    if (!item) {
      return 1;
    }
    
    limit = item.meta.StackLimit || 0;
    
    if (DataManager.isItem(item)) {
      typeItem = 'i';
      limit = this._modStackLimit._items[item.id] || limit;
    } else if (DataManager.isWeapon(item)) {
      typeItem = 'w';
      limit = this._modStackLimit._weapons[item.id] || limit;
    } else if (DataManager.isArmor(item)) {
      typeItem = 'a';
      limit = this._modStackLimit._armors[item.id] || limit;
    } else {
      return 1;
    }

    if(limit > 0)
      return limit;
    
    limit = item.meta.StackLimit || 0;

    if(limit > 0)
      return limit;
    if(typeItem === 'i')
      return paramItemStackDefault;
    if(typeItem === 'w')
      return paramWeaponStackDefault;
    if(typeItem === 'a')
      return paramArmorStackDefault;
    return 1;
  };
 
  /**********************************************************************
  * Méthode : numItemStack (ajout)
  * Fonction : retourne le nombre de stack pris par un objet
  * Params : item : objet dont il faut numérer les stack
  *          offset : décalage du nombre d'objet
  **********************************************************************/
  Game_Party.prototype.numItemStack = function(item, offset) {
    if((this.numItems(item) + (offset || 0)) === 0)
      return 0;
    return Math.floor((this.numItems(item)-1 + (offset || 0))/ this.stackLimit(item)) + 1;
  };
 
  /**********************************************************************
  * Méthode : initAllItems (surcharge)
  * Fonction : Ajoute la liste des recettes
  * Params : --
  **********************************************************************/
  var _Game_Party_initAllItems = Game_Party.prototype.initAllItems;
  Game_Party.prototype.initAllItems = function() {
    _Game_Party_initAllItems.call(this);
    this._bank = {};
    this._bagLimit = paramNbrOfDiffItem;
    this._modStackLimit = {};
    this._modStackLimit._items = {};
    this._modStackLimit._weapons = {};
    this._modStackLimit._armors = {};
    
    this._bankSelect = 'one';
    this._listBank = {};
    this.setBank();
  };
  
  /**********************************************************************
  * Méthode : maxItems (remplacement)
  * Fonction : modifie la limite du nombre d'objet à 999999
  * Params : item : objet dont il faut connaitre la limite
  **********************************************************************/
  Game_Party.prototype.maxItems = function(item) {
    return 999999;
  };
  
  /**********************************************************************
  * Méthode : gainItem (surcharge)
  * Fonction : Ajoute ou retire une recette de la liste des recettes
  * Params : item : objet a ajouter dans l'inventaire
  *          amount : nombre d'ojet à ajouter
  *          includeEquip : flag indiquant si c'est une pièce à équiper
  **********************************************************************/
  var _Game_Party_gainItem = Game_Party.prototype.gainItem;
  Game_Party.prototype.gainItem = function(item, amount, includeEquip) {
    if(amount < 0)
      _Game_Party_gainItem.call(this, item, amount, includeEquip);
    else{
      var stackLimit = this.stackLimit(item);
      var numItemStack = this.numItemStack(item);
      var stackPlus = this.numItemStack(item, amount) - numItemStack;
      if(this.numAllStack() + stackPlus <= this._bagLimit)
        _Game_Party_gainItem.call(this, item, amount, includeEquip);
      else{
        stackPlus = this._bagLimit - this.numAllStack();
        _Game_Party_gainItem.call(this, item, ((numItemStack + stackPlus) * stackLimit) - this.numItems(item), includeEquip);
      }
    }
  };

  /**********************************************************************
  * Méthode : setBank (ajout)
  * Fonction : permet de se positionner sur une banque
  * Params : pNomBank : code optionnel d'un espace de stockage
  **********************************************************************/
  Game_Party.prototype.setBank = function(pNomBank) {
    this._bankSelect = pNomBank || 'one';
    if(!this._listBank[this._bankSelect]){
      this._listBank[this._bankSelect] = {};
      this._listBank[this._bankSelect]._bankLimit = paramBankNbrOfDiffItem;
      this._listBank[this._bankSelect]._items = {};
      this._listBank[this._bankSelect]._weapons = {};
      this._listBank[this._bankSelect]._armors = {};
      this._listBank[this._bankSelect]._modStackLimit = {};
      this._listBank[this._bankSelect]._modStackLimit._items = {};
      this._listBank[this._bankSelect]._modStackLimit._weapons = {};
      this._listBank[this._bankSelect]._modStackLimit._armors = {};
    }
  };
  
  /**********************************************************************
  * Méthode : bankLimit (ajout)
  * Fonction : permet de retourner la limite de stack de l'espace de stockage en cours
  * Params : --
  **********************************************************************/
  Game_Party.prototype.bankLimit = function() {
    return this._listBank[this._bankSelect]._bankLimit;
  };
  
  /**********************************************************************
  * Méthode : bankItems (ajout)
  * Fonction : retourne un tableau contenant les objets du stockage
  * Params : --
  **********************************************************************/
  Game_Party.prototype.bankItems = function() {
    var list = [];
    for (var id in this._listBank[this._bankSelect]._items) {
        list.push($dataItems[id]);
    }
    return list;
  };
  
  /**********************************************************************
  * Méthode : bankWeapons (ajout)
  * Fonction : retourne les armes stockés
  * Params : --
  **********************************************************************/
  Game_Party.prototype.bankWeapons = function() {
    var list = [];
    for (var id in this._listBank[this._bankSelect]._weapons) {
        list.push($dataWeapons[id]);
    }
    return list;
  };

  /**********************************************************************
  * Méthode : bankArmors (ajout)
  * Fonction : retourne les pièces d'armure du stockage
  * Params : --
  **********************************************************************/
  Game_Party.prototype.bankArmors = function() {
    var list = [];
    for (var id in this._listBank[this._bankSelect]._armors) {
        list.push($dataArmors[id]);
    }
    return list;
  };

  /**********************************************************************
  * Méthode : bankEquipItems (ajout)
  * Fonction : retourne les armes et armures du stockage
  * Params : --
  **********************************************************************/
  Game_Party.prototype.bankEquipItems = function() {
    return this.bankWeapons().concat(this.bankArmors());
  };

  /**********************************************************************
  * Méthode : bankAllItems (ajout)
  * Fonction : retourne tous le stockage
  * Params : pNomBank : code optionnel d'un espace de stockage
  **********************************************************************/
  Game_Party.prototype.bankAllItems = function() {
    return this.bankItems().concat(this.bankEquipItems());
  };
  
  /**********************************************************************
  * Méthode : bankNumAllStack (ajout)
  * Fonction : retourne le nombre de stack dans la banque sélectionnée
  * Params : --
  **********************************************************************/
  Game_Party.prototype.bankNumAllStack = function() {
    // pour chaque objet possédés
    var n = 0;
    $gameParty.bankAllItems().forEach(function(item) {
      n += $gameParty.bankNumItemStack(item);
    });
    return n;
  };
  
  /**********************************************************************
  * Méthode : bankFull (ajout)
  * Fonction : permet se savoir si la banque sélectionnée est pleine
  * Params : --
  **********************************************************************/
  Game_Party.prototype.bankFull = function() {
    return (this.bankNumAllStack() >= this.bankLimit() );
  };
  
  /**********************************************************************
  * Méthode : bankItemContainer (ajout)
  * Fonction : retourne le conteneur de l'objet transmis
  * Params : item : objet
  **********************************************************************/
  Game_Party.prototype.bankItemContainer = function(item) {
    if (!item) {
        return null;
    } else if (DataManager.isItem(item)) {
        return this._listBank[this._bankSelect]._items;
    } else if (DataManager.isWeapon(item)) {
        return this._listBank[this._bankSelect]._weapons;
    } else if (DataManager.isArmor(item)) {
        return this._listBank[this._bankSelect]._armors;
    } else {
        return null;
    }
  };
  
  /**********************************************************************
  * Méthode : bankGainItem (ajout)
  * Fonction : permet d'ajouter un objet dans l'espace de stockage
  * Params : item : objet à ajouter
  *          amount : nombre d'objet
  **********************************************************************/
  Game_Party.prototype.bankGainItem = function(item, amount) {
    if(amount > 0){
      var stackLimit = this.stackLimit(item);
      var numItemStack = this.numItemStack(item);
      var stackPlus = this.numItemStack(item, amount) - numItemStack;
      if(this.bankNumAllStack() + stackPlus > this._listBank[this._bankSelect]._bankLimit){
        stackPlus = this._listBank[this._bankSelect]._bankLimit - this.bankNumAllStack();
        amount = ((numItemStack + stackPlus) * stackLimit) - this.bankNumItems(item);
      }
    }
    if(amount !== 0){
      var container = this.bankItemContainer(item);
      if (container) {
        var lastNumber = this.bankNumItems(item);
        var newNumber = lastNumber + amount;
        container[item.id] = newNumber.clamp(0, this.maxItems(item));
        if (container[item.id] === 0) {
          delete container[item.id];
        }
      }
    }
  };
  
  /**********************************************************************
  * Méthode : bankNumItems (ajout)
  * Fonction : permet d'ajouter un objet dans l'espace de stockage
  * Params : item : objet à ajouter
  **********************************************************************/
  Game_Party.prototype.bankNumItems = function(item) {
    var container = this.bankItemContainer(item);
    return container ? container[item.id] || 0 : 0;
  };
  
  /**********************************************************************
  * Méthode : bankNumItemStack (ajout)
  * Fonction : retourne le nombre de stack pris par un objet 
  * Params : item : objet dont il faut numérer les stack
  *          offset : décalage du nombre d'objet
  **********************************************************************/
  Game_Party.prototype.bankNumItemStack = function(item, offset) {
    if((this.bankNumItems(item) + (offset || 0)) === 0)
      return 0;
    return Math.floor((this.bankNumItems(item)-1 + (offset || 0))/ this.stackLimit(item)) + 1;
  };
  
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création de la "classe" Window_ItemAction
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_ItemAction() {
    this.initialize.apply(this, arguments);
  }

  Window_ItemAction.prototype = Object.create(Window_HorzCommand.prototype);
  Window_ItemAction.prototype.constructor = Window_ItemAction;

  /**********************************************************************
  * Méthode : initialize
  * Fonction : initialisation de la fenêtre
  * Params : --
  **********************************************************************/
  Window_ItemAction.prototype.initialize = function() {
    Window_HorzCommand.prototype.initialize.call(this, 0, 0);
    this.visible = false;
  };

  /**********************************************************************
  * Méthode : windowWidth
  * Fonction : retourne la largeur de la fenêtre de jeu
  * Params : --
  **********************************************************************/
  Window_ItemAction.prototype.windowWidth = function() {
    return Graphics.boxWidth;
  };

  /**********************************************************************
  * Méthode : maxCols
  * Fonction : retourne le nombre max de colonne de la fenêtre
  * Params : --
  **********************************************************************/
  Window_ItemAction.prototype.maxCols = function() {
    return 2;
  };

  /**********************************************************************
  * Méthode : makeCommandList
  * Fonction : cré la liste des commande du menu
  * Params : --
  **********************************************************************/
  Window_ItemAction.prototype.makeCommandList = function() {
    this.addCommand(paramNameUseOption,    'use');
    this.addCommand(paramNameDeleteOption, 'delete');
  };

  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création d'une nouvelle "classe" Window_ItemCount
  *  hérite de la classe Window_Base
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_ItemCount() {
    this.initialize.apply(this, arguments);
  }

  Window_ItemCount.prototype = Object.create(Window_Base.prototype);
  Window_ItemCount.prototype.constructor = Window_ItemCount;

  /**********************************************************************
  * Méthode : initialize
  * Fonction : initialisation de la fenêtre
  * Params : --
  **********************************************************************/
  Window_ItemCount.prototype.initialize = function(pX, pY) {
    Window_Base.prototype.initialize.call(this, pX, pY, 100, this.fittingHeight(1));
    this._max = 0;//$gameParty._limitNbrItem;
    this._number = 0;//$gameParty.numAllStack();
    this.backOpacity = 0;
    this.opacity = 0;
    this.refresh();
  };
  
  /**********************************************************************
  * Méthode : refresh
  * Fonction : permet de recréer le contenu de la fenêtre
  * Params : --
  **********************************************************************/
  Window_ItemCount.prototype.refresh = function(){
    this.contents.clear();
    this.drawText(String(this._number)+'/'+String(this._max), 0, 0, this.contentsWidth());
  };
  
  /**********************************************************************
  * Méthode : setValues
  * Fonction : permet de modifier la valeur courante et la valeur Max
  * Params : --
  **********************************************************************/
  Window_ItemCount.prototype.setValues = function(pCurrent, pMax){
    if(this._max !== pMax || this._number !== pCurrent ){
      this._max = pMax;
      this._number = pCurrent;
      this.refresh();
    }
  };
 

  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Window_ItemList
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : initialize (surcharge)
  * Fonction : initialisation de la fenêtre
  * Params : --
  **********************************************************************/
  var _Window_ItemList_initialize = Window_ItemList.prototype.initialize;
  Window_ItemList.prototype.initialize = function(x, y, width, height) {
    _Window_ItemList_initialize.call(this, x, y, width, height);
    this._dataNumItem = [];
    this._itemCountWindow = new Window_ItemCount();
    this._itemCountWindow.x = this.width - this._itemCountWindow.width + 14;
    this._itemCountWindow.y = this.height - this._itemCountWindow.height + 14;
    this.addChild(this._itemCountWindow);
  };

  /**********************************************************************
  * Méthode : spacing (ajout)
  * Fonction : définie à 12 l'espacement entre les objets
  * Params : --
  **********************************************************************/
  Window_ItemList.prototype.spacing = function() {
    return 12;
  };
  
  /**********************************************************************
  * Méthode : itemWidth (ajout)
  * Fonction : modifie la largeur de la sélection si compteur présent
  * Params : --
  **********************************************************************/
  Window_ItemList.prototype.itemWidth = function() {
    var newWidth = Window_Selectable.prototype.itemWidth.call(this);
    if(this._itemCountWindow)
      newWidth -= ((this.width - this._itemCountWindow.x) / this.maxCols());
    return newWidth;
  };

  /**********************************************************************
  * Méthode : includes (surcharge)
  * Fonction : permet de gérer la catégorie all
  * Params : --
  **********************************************************************/
  var _Window_ItemList_includes = Window_ItemList.prototype.includes;
  Window_ItemList.prototype.includes = function(item) {
    if(!item)
      return false;
    if(this._category !== 'all')
      return _Window_ItemList_includes.call(this, item);
    else{
      return true;
    }
  };

  /**********************************************************************
  * Méthode : setCategory (surcharge)
  * Fonction : permet d'afficher tous les objets si tout est groupé
  * Params : --
  **********************************************************************/
  var _Window_ItemList_setCategory = Window_ItemList.prototype.setCategory;
  Window_ItemList.prototype.setCategory = function(category) {
    if(paramGroupAllItemType === 1)
      category = 'all';
    _Window_ItemList_setCategory.call(this, category);
  };
  
  /**********************************************************************
  * Méthode : setMode (ajout)
  * Fonction : permet de changer le mode d'affichage
  * Params : --
  **********************************************************************/
  Window_ItemList.prototype.setMode = function(mode) {
    if(this._mode !== mode){
      this._mode = mode;
      this.refresh();
    }
  };
  
  /**********************************************************************
  * Méthode : isEnabled (surcharge)
  * Fonction : permet de rendre actif tous les objets si pas en mode 'use'
  * Params : --
  **********************************************************************/
  var _Window_ItemList_isEnabled = Window_ItemList.prototype.isEnabled;
  Window_ItemList.prototype.isEnabled = function(item) {
    if((this._mode || 'use') === 'use')
      return _Window_ItemList_isEnabled.call(this, item);
    return true;
  };
  
  /**********************************************************************
  * Méthode : getAllItems (ajout)
  * Fonction : permet de récupérer tous les objets de l'inventaire
  * Params : --
  **********************************************************************/
  Window_ItemList.prototype.getAllItems = function() {
    return $gameParty.allItems();
  };
  
  /**********************************************************************
  * Méthode : getNumItem (ajout)
  * Fonction : permet connaitre la quantité de l'objet
  * Params : item : objet à dénombrer
  **********************************************************************/
  Window_ItemList.prototype.getNumItem = function(item) {
    return $gameParty.numItems(item);
  };
  
  /**********************************************************************
  * Méthode : makeItemList (surcharge)
  * Fonction : construit la liste des objets affichables
  * Params : --
  **********************************************************************/
  Window_ItemList.prototype.makeItemList = function() {
    this._data = [];
    this._dataNumItem = [];
    var dataItemParty = [];
    dataItemParty = this.getAllItems().filter(function(item) {
      return this.includes(item);
    }, this);
    if (this.includes(null)) {
      dataItemParty.push(null);
    }
    
    for(var i = 0; i < dataItemParty.length; i++) {
      var item = dataItemParty[i];
      if(item === null)
        this._data.push(item);
      else{
        var stackLimit = $gameParty.stackLimit(item);
        var num = this.getNumItem(item);
        while(num > 0) {
          var stackSize = Math.min(stackLimit, num);
          this._data.push(item);
          this._dataNumItem.push(stackSize);
          num -= stackLimit;
        }
      }
    }
  };
  
  /**********************************************************************
  * Méthode : drawItem (surcharge)
  * Fonction : ajoute la gestion du nombre d'objet sur un stack
  * Params : --
  **********************************************************************/
  var _Window_ItemList_drawItem = Window_ItemList.prototype.drawItem;
  Window_ItemList.prototype.drawItem = function(index) {
    this._numItemSave = this._dataNumItem[index];
    _Window_ItemList_drawItem.call(this, index);
  };
  
  /**********************************************************************
  * Méthode : drawItemNumber (surcharge)
  * Fonction : prend en charge la propriété du nombre d'objet d'un stack
  * Params : --
  **********************************************************************/
  Window_ItemList.prototype.drawItemNumber = function(item, x, y, width) {
    if (this.needsNumber()) {
      this.drawText(':', x, y, width - this.textWidth('00'), 'right');
      this.drawText(this._numItemSave, x, y, width, 'right');
    }
  };
  
  /**********************************************************************
  * Méthode : updateCountWindow (ajout)
  * Fonction : ajoute la mise à jour du compteur
  * Params : --
  **********************************************************************/
  Window_ItemList.prototype.updateCountWindow = function() {
    this._itemCountWindow.setValues($gameParty.numAllStack(), $gameParty._bagLimit);
  };
  
  /**********************************************************************
  * Méthode : update (surcharge)
  * Fonction : ajoute la gestion du nombre d'objet sur un stack
  * Params : --
  **********************************************************************/
  var _Window_ItemList_update = Window_ItemList.prototype.update;
  Window_ItemList.prototype.update = function() {
    if(this._itemCountWindow){
      if(this._itemCountWindow.x !== this.width - this._itemCountWindow.width + 14)
        this._itemCountWindow.x = this.width - this._itemCountWindow.width + 14;
      if(this._itemCountWindow.y !== this.height - this._itemCountWindow.height + 14)
        this._itemCountWindow.y = this.height - this._itemCountWindow.height + 14;
      this.updateCountWindow();
    }
    _Window_ItemList_update.call(this);
  };

  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Scene_Item
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : create (surcharge)
  * Fonction : Ajoute l'appel à la fonction de création de la fenêtre ItemCount
  * Params : --
  **********************************************************************/
  var _Scene_Item_create = Scene_Item.prototype.create;
  Scene_Item.prototype.create = function() {
    _Scene_Item_create.call(this);
    this.createItemActionWindow();
    
    if(paramGroupAllItemType === 1){
      this._categoryWindow.visible = false;
      this._itemWindow.y -= this._categoryWindow.height;
      this._itemWindow.height += this._categoryWindow.height;
      this._itemWindow.setCategory('all');
    }
    if(paramAddMenuDelete === 1){
      this._itemActionWindow.visible = true;
      this._itemWindow.y += this._itemActionWindow.height;
      this._itemWindow.height -= this._itemActionWindow.height;
    }
    
    this._itemWindow.deactivate();
    this._itemActionWindow.deactivate();
    this._categoryWindow.deactivate();
    if(paramGroupAllItemType === 1 && paramAddMenuDelete !== 1){
      this._itemWindow.activate();
      this._itemWindow.selectLast();
    }else if(paramGroupAllItemType === 1){
      this._itemActionWindow.select(0);
      this._itemActionWindow.activate();
    }else{
      this._itemActionWindow.deselect();
      this._categoryWindow.activate();
    }
    
  };
  
  /**********************************************************************
  * Méthode : createItemActionWindow (ajout)
  * Fonction : Ajoute l'appel à la fonction de création de la fenêtre ItemCount
  * Params : --
  **********************************************************************/
  Scene_Item.prototype.createItemActionWindow = function() {
    this._itemActionWindow = new Window_ItemAction();
    this._itemActionWindow.y = this._helpWindow.height + (paramGroupAllItemType === 0 ? this._categoryWindow.height : 0);
    this._itemActionWindow.setHandler('ok', this.onActionOk.bind(this));
    this._itemActionWindow.setHandler('cancel', this.onActionCancel.bind(this));
    this._windowLayer.addChildAt(this._itemActionWindow, 0);

  };
  
  /**********************************************************************
  * Méthode : onCategoryOk (surcharge)
  * Fonction : ajoute la gestion de suppression
  * Params : --
  **********************************************************************/
  var _Scene_Item_onCategoryOk = Scene_Item.prototype.onCategoryOk;
  Scene_Item.prototype.onCategoryOk = function() {
    if(paramAddMenuDelete !== 1)
      _Scene_Item_onCategoryOk.call(this);
    else{
      this._itemActionWindow.select(0);
      this._itemActionWindow.activate();
    }
  };
  
  /**********************************************************************
  * Méthode : onActionOk (ajout)
  * Fonction : gère la validation du menu d'action
  * Params : --
  **********************************************************************/
  Scene_Item.prototype.onActionOk = function() {
    this._itemWindow.setMode(this._itemActionWindow.currentSymbol());
    this._itemWindow.activate();
    this._itemWindow.selectLast();
  };
  
  /**********************************************************************
  * Méthode : onActionCancel (ajout)
  * Fonction : gère l'annulation du menu d'action
  * Params : --
  **********************************************************************/
  Scene_Item.prototype.onActionCancel = function() {
    this._itemActionWindow.deselect();
    if(paramGroupAllItemType === 1)
      this.popScene();
    else{
      this._categoryWindow.activate();
    }
  };
  
  /**********************************************************************
  * Méthode : onItemOk (surcharge)
  * Fonction : ajoute la gestion de la suppression
  * Params : --
  **********************************************************************/
  var _Scene_Item_onItemOk = Scene_Item.prototype.onItemOk;
  Scene_Item.prototype.onItemOk = function() {
    if(this._itemActionWindow.currentSymbol() === 'use'){
      _Scene_Item_onItemOk.call(this);
    }else{
      // TODO voir pour ajouter une fenêtre de spécification du nombre
      $gameParty.loseItem(this.item(), 1);
      this._itemWindow.refresh();
      this._itemWindow.activate();
      if(this._itemWindow.index() === this._itemWindow.maxItems() && this._itemWindow.index() !== 0)
        this._itemWindow.select(this._itemWindow.maxItems()-1);
    }
  };
  
  /**********************************************************************
  * Méthode : onItemCancel (surcharge)
  * Fonction : ajoute la gestion du menu d'action
  * Params : --
  **********************************************************************/
  Scene_Item.prototype.onItemCancel = function() {
    this._itemWindow.deselect();
    if(paramGroupAllItemType === 1 && paramAddMenuDelete !== 1){
      this.popScene();
    }else if(paramAddMenuDelete === 1){
      this._itemActionWindow.activate();
    }else{
      this._categoryWindow.activate();
    }
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Scene_Equip
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : onItemOk (surcharge)
  * Fonction : Ajoute l'appel à la fonction de création de la fenêtre ItemCount
  * Params : --
  **********************************************************************/
  var _Scene_Equip_onItemOk = Scene_Equip.prototype.onItemOk;
  Scene_Equip.prototype.onItemOk = function() {
    if(this.actor().changeEquipPossible(this._slotWindow.index(), this._itemWindow.item()))
      _Scene_Equip_onItemOk.call(this);
    else{
      SoundManager.playBuzzer();
      this._itemWindow.activate();
    }
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création de la "classe" Window_ItemActionBank
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_ItemActionBank() {
    this.initialize.apply(this, arguments);
  }

  Window_ItemActionBank.prototype = Object.create(Window_HorzCommand.prototype);
  Window_ItemActionBank.prototype.constructor = Window_ItemActionBank;

  /**********************************************************************
  * Méthode : initialize
  * Fonction : initialisation de la fenêtre
  * Params : --
  **********************************************************************/
  Window_ItemActionBank.prototype.initialize = function() {
    Window_HorzCommand.prototype.initialize.call(this, 0, 0);
  };

  /**********************************************************************
  * Méthode : windowWidth
  * Fonction : retourne la largeur de la fenêtre de jeu
  * Params : --
  **********************************************************************/
  Window_ItemActionBank.prototype.windowWidth = function() {
    return Graphics.boxWidth;
  };

  /**********************************************************************
  * Méthode : maxCols
  * Fonction : retourne le nombre max de colonne de la fenêtre
  * Params : --
  **********************************************************************/
  Window_ItemActionBank.prototype.maxCols = function() {
    return 4;
  };

  /**********************************************************************
  * Méthode : makeCommandList
  * Fonction : cré la liste des commande du menu
  * Params : --
  **********************************************************************/
  Window_ItemActionBank.prototype.makeCommandList = function() {
    this.addCommand(paramNamePutOption, 'put');
    this.addCommand(paramNameGetOption, 'get');
    this.addCommand(paramNameBag + ' ' + paramNameDeleteOption, 'deleteBag');
    this.addCommand(paramNameBank + ' ' + paramNameDeleteOption, 'deleteBank');
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création de la "classe" Window_ItemListBag
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_ItemListBag() {
    this.initialize.apply(this, arguments);
  }

  Window_ItemListBag.prototype = Object.create(Window_ItemList.prototype);
  Window_ItemListBag.prototype.constructor = Window_ItemListBag;

  /**********************************************************************
  * Méthode : maxCols
  * Fonction : force maxCols à 1
  * Params : --
  **********************************************************************/
  Window_ItemListBag.prototype.maxCols = function() {
    return 1;
  };
  
  /**********************************************************************
  * Méthode : isEnabled
  * Fonction : force isEnabled à true
  * Params : --
  **********************************************************************/
  Window_ItemListBag.prototype.isEnabled = function(item) {
    return true;
  };
  
  /**********************************************************************
  * Méthode : playOkSound
  * Fonction : force playOkSound à rien
  * Params : --
  **********************************************************************/
  Window_ItemListBag.prototype.playOkSound = function() {
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création de la "classe" Window_ItemListBank
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_ItemListBank() {
    this.initialize.apply(this, arguments);
  }

  Window_ItemListBank.prototype = Object.create(Window_ItemListBag.prototype);
  Window_ItemListBank.prototype.constructor = Window_ItemListBank;
  
  /**********************************************************************
  * Méthode : initialize
  * Fonction : initialisation de la fenêtre
  * Params : --
  **********************************************************************/
  Window_ItemListBank.prototype.initialize = function(x, y, width, height) {
    Window_ItemListBag.prototype.initialize.call(this, x, y, width, height);
  };
  
  /**********************************************************************
  * Méthode : updateCountWindow
  * Fonction : initialisation de la fenêtre
  * Params : --
  **********************************************************************/
  Window_ItemListBank.prototype.updateCountWindow = function() {
    this._itemCountWindow.setValues($gameParty.bankNumAllStack(), $gameParty.bankLimit());
  };
  
  /**********************************************************************
  * Méthode : getAllItems
  * Fonction : récupère le contenu du stockage actif
  * Params : --
  **********************************************************************/
  Window_ItemListBank.prototype.getAllItems = function() {
    return $gameParty.bankAllItems();
  };
  
  /**********************************************************************
  * Méthode : getNumItem
  * Fonction : récupère la quantité de l'objet dans le stockage
  * Params : item : objet à dénombrer
  **********************************************************************/
  Window_ItemListBank.prototype.getNumItem = function(item) {
    return $gameParty.bankNumItems(item);
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création de la "classe" Scene_Bank
  *----------------------------------------------------------------------
  **********************************************************************/
  function Scene_Bank() {
    this.initialize.apply(this, arguments);
  }

  Scene_Bank.prototype = Object.create(Scene_MenuBase.prototype);
  Scene_Bank.prototype.constructor = Scene_Bank;

  /**********************************************************************
  * Méthode : initialize
  * Fonction : Initialisation de la "classe" Scene_Bank
  * Params : --
  **********************************************************************/
  Scene_Bank.prototype.initialize = function() {
    Scene_MenuBase.prototype.initialize.call(this);
    this._mode = "";
  };
  
  /**********************************************************************
  * Méthode : create (surcharge)
  * Fonction : Permet d'appeler les fonctions de construction de la scene
  * Params : --
  **********************************************************************/
  Scene_Bank.prototype.create = function() {
    Scene_MenuBase.prototype.create.call(this);
    this.createHelpWindow();
    this.createCategoryWindow();
    this.createItemActionWindow();
    this.createItemWindow();

    if(paramGroupAllItemType === 1){
      this._categoryWindow.visible = false;
      this._itemWindowBag.setCategory('all');
      this._itemWindowBank.setCategory('all');
    }
    
    this._itemActionWindow.deactivate();
    this._categoryWindow.deactivate();
    if(paramGroupAllItemType === 1){
      this._itemActionWindow.select(0);
      this._itemActionWindow.activate();
    }else{
      this._itemActionWindow.deselect();
      this._categoryWindow.activate();
    }
    
  };

  /**********************************************************************
  * Méthode : createCategoryWindow (ajout)
  * Fonction : Construit la fenêtre de catégorie
  * Params : --
  **********************************************************************/
  Scene_Bank.prototype.createCategoryWindow = function() {
    this._categoryWindow = new Window_ItemCategory();
    this._categoryWindow.setHelpWindow(this._helpWindow);
    this._categoryWindow.y = this._helpWindow.height;
    this._categoryWindow.setHandler('ok',     this.onCategoryOk.bind(this));
    this._categoryWindow.setHandler('cancel', this.popScene.bind(this));
    this.addWindow(this._categoryWindow);
  };
  
  /**********************************************************************
  * Méthode : createItemActionWindow (ajout)
  * Fonction : Ajoute l'appel à la fonction de création de la fenêtre ItemCount
  * Params : --
  **********************************************************************/
  Scene_Bank.prototype.createItemActionWindow = function() {
    this._itemActionWindow = new Window_ItemActionBank();
    this._itemActionWindow.y = this._helpWindow.height + (paramGroupAllItemType === 0 ? this._categoryWindow.height : 0);
    this._itemActionWindow.setHandler('put', this.onActionPut.bind(this));
    this._itemActionWindow.setHandler('get', this.onActionGet.bind(this));
    this._itemActionWindow.setHandler('deleteBag', this.onActionDelBag.bind(this));
    this._itemActionWindow.setHandler('deleteBank', this.onActionDelBank.bind(this));
    this._itemActionWindow.setHandler('cancel', this.onActionCancel.bind(this));
    this.addWindow(this._itemActionWindow);
    //this._windowLayer.addChildAt(this._itemActionWindow, 0);
  };

  /**********************************************************************
  * Méthode : createItemWindow (ajout)
  * Fonction : Ajoute les fenêtre de liste d'objet
  * Params : --
  **********************************************************************/
  Scene_Bank.prototype.createItemWindow = function() {
    var wy = this._itemActionWindow.y + this._itemActionWindow.height;
    var wh = Graphics.boxHeight - wy;
    this._itemWindowBag = new Window_ItemListBag(0, wy, Graphics.boxWidth / 2, wh);
    this._itemWindowBag.setHelpWindow(this._helpWindow);
    this._itemWindowBag.setHandler('ok',     this.onItemBagOk.bind(this));
    this._itemWindowBag.setHandler('cancel', this.onItemBagCancel.bind(this));
    this.addWindow(this._itemWindowBag);
    //this._categoryWindow.setItemWindow(this._itemWindow); // gérer le changement dans l'update de la scene
    
    this._itemWindowBank = new Window_ItemListBank(Graphics.boxWidth / 2, wy, Graphics.boxWidth / 2, wh);
    this._itemWindowBank.setHelpWindow(this._helpWindow);
    this._itemWindowBank.setHandler('ok',     this.onItemBankOk.bind(this));
    this._itemWindowBank.setHandler('cancel', this.onItemBankCancel.bind(this));
    this.addWindow(this._itemWindowBank);
  };
  
  /**********************************************************************
  * Méthode : onCategoryOk (ajout)
  * Fonction : action sur validation de la fenêtre catégorie
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onCategoryOk = function() {
    this._itemWindowBag.setCategory(this._categoryWindow.currentSymbol());
    this._itemWindowBank.setCategory(this._categoryWindow.currentSymbol());
    this._itemActionWindow.select(0);
    this._itemActionWindow.activate();
  };
  
  /**********************************************************************
  * Méthode : onActionPut (ajout)
  * Fonction : action de dépot en stockage
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onActionPut = function() {
    this._itemWindowBag.select(0);
    this._itemWindowBag.activate();
    this._mode = "put";
  };
  
  /**********************************************************************
  * Méthode : onActionGet (ajout)
  * Fonction : action de récupération du stockage
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onActionGet = function() {
    this._itemWindowBank.select(0);
    this._itemWindowBank.activate();
    this._mode = "get";
  };
  
  /**********************************************************************
  * Méthode : onActionDelBag (ajout)
  * Fonction : action de suppression d'un objet de l'inventaire
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onActionDelBag = function() {
    this._itemWindowBag.select(0);
    this._itemWindowBag.activate();
    this._mode = "del_bag";
  };
    
  /**********************************************************************
  * Méthode : onActionDelBank (ajout)
  * Fonction : action de suppresion d'un objet du stockage
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onActionDelBank = function() {
    this._itemWindowBank.select(0);
    this._itemWindowBank.activate();
    this._mode = "del_bank";
  };
  
  /**********************************************************************
  * Méthode : onActionCancel (ajout)
  * Fonction : action d'annulation de choix d'action
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onActionCancel = function() {
    this._itemActionWindow.deselect();
    if(paramGroupAllItemType === 1)
      this.popScene();
    else{
      this._categoryWindow.activate();
      this._itemWindowBag.setCategory();
      this._itemWindowBank.setCategory();
    }
  };

  /**********************************************************************
  * Méthode : onItemBagOk (ajout)
  * Fonction : action sur validation d'un objet de la fenêtre d'inventaire
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onItemBagOk = function() {
    if(this._mode === "put" ){
      // Vérifier que le coffre ne soit pas plein avec le nouvel objet
      var offsetStack = $gameParty.bankNumItemStack(this._itemWindowBag.item(), 1) - $gameParty.bankNumItemStack(this._itemWindowBag.item());
      if($gameParty.bankNumAllStack() + offsetStack >= $gameParty.bankLimit())
        SoundManager.playBuzzer();
      else{
        SoundManager.playOk();
        $gameParty.loseItem(this._itemWindowBag.item(), 1);
        $gameParty.bankGainItem(this._itemWindowBag.item(), 1);
        this._itemWindowBag.refresh();
        this._itemWindowBank.refresh();
      }
      this._itemWindowBag.activate();
       
      
    }else if(this._mode === "del_bag" ){
      // TODO voir pour ajouter une fenêtre de spécification du nombre
      SoundManager.playOk();
      $gameParty.loseItem(this._itemWindowBag.item(), 1);
      this._itemWindowBag.refresh();
      this._itemWindowBag.activate();
      if(this._itemWindowBag.index() === this._itemWindowBag.maxItems() && this._itemWindowBag.index() !== 0)
        this._itemWindowBag.select(this._itemWindowBag.maxItems()-1);
    }
  };
  
  /**********************************************************************
  * Méthode : onItemBagCancel (ajout)
  * Fonction : action sur annulation fenêtre inventaire
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onItemBagCancel = function() {
    this._itemWindowBag.deselect();
    this._itemActionWindow.activate();
    this._mode = "";
  };
  
  /**********************************************************************
  * Méthode : onItemBankOk (ajout)
  * Fonction : action sur validation objet dans stockage
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onItemBankOk = function() {
    if(this._mode === "get" ){
      var offsetStack = $gameParty.numItemStack(this._itemWindowBank.item(), 1) - $gameParty.numItemStack(this._itemWindowBank.item());
      if($gameParty.numAllStack() + offsetStack >= $gameParty._bagLimit)
        SoundManager.playBuzzer();
      else{
        SoundManager.playOk();
        $gameParty.bankGainItem(this._itemWindowBank.item(), -1);
        $gameParty.gainItem(this._itemWindowBank.item(), 1);
        this._itemWindowBag.refresh();
        this._itemWindowBank.refresh();
      }
      this._itemWindowBank.activate();
      
    }else if(this._mode === "del_bank" ){
      // TODO voir pour ajouter une fenêtre de spécification du nombre
      SoundManager.playOk();
      $gameParty.bankGainItem(this._itemWindowBank.item(), -1);
      this._itemWindowBank.refresh();
      this._itemWindowBank.activate();
      if(this._itemWindowBank.index() === this._itemWindowBank.maxItems() && this._itemWindowBank.index() !== 0)
        this._itemWindowBank.select(this._itemWindowBank.maxItems()-1);
    }
  };
  
  /**********************************************************************
  * Méthode : onItemBankCancel (ajout)
  * Fonction : action sur annulation dans fenêtre de stockage
  * Params : --
  **********************************************************************/  
  Scene_Bank.prototype.onItemBankCancel = function() {
    this._itemWindowBank.deselect();
    this._itemActionWindow.activate();
    this._mode = "";
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Interpreter
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : pluginCommand (surcharge)
  * Fonction : Ajoute la commande 
  * Params : --
  **********************************************************************/
  var _Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
  Game_Interpreter.prototype.pluginCommand = function(command, args) {
    _Game_Interpreter_pluginCommand.call(this, command, args);
    if(command === 'LBB_openBank'){
      $gameParty.setBank(args[0]||'one');
      SceneManager.push(Scene_Bank);
    }else if(command === 'LBB_changeLimitBag'){
      $gameParty._bagLimit = args[0]||paramNbrOfDiffItem;
    }
  };
  
})();

