//=============================================================================
// Tonyryu_Alchimie.js
//=============================================================================

/*:
 * @plugindesc Plugin permettant de gérer l'alchimie de façon très simple (1.05)
 * @author Tonyryu
 *
 * @param Dans menu
 * @desc 0 : Non, 1 : Option ajoutée dans le menu
 * @default 1
 *
 * @param Nom option
 * @desc Nom de l'option d'alchimie dans le menu
 * @default Alchimie
 *
 * @param Affichage
 * @desc 0 : au moins un composant, 1 : tout afficher
 * @default 0
 *
 * @param AchatRecettes
 * @desc 0 : toutes dispos, 1 : doivent être acheter
 * @default 0
 *
 * @help http://www.tonyryudev.com/
 * 
 */

/*
 * Suivi de version 
 * 1.00 : 28/10/2015 : Tonyryu : Création plugin
 * 1.01 : 28/10/2015 : Tonyryu : Correction passage de paramètre au GameInterpreter
 * 1.02 : 12/11/2015 : Tonyryu : Ajout d'une vérification d'existance de l'objet avant récupération note
 * 1.03 : 11/03/2015 : Tonyryu : Ajout de la possibilité d'acheter les recettes
 * 1.04 : 17/03/2016 : Tonyryu : Correction d'un bug lors du déséquipement d'un objet
 * 1.05 : 29/03/2016 : Tonyryu : Correction bug d'ouverture du menu via commande de module
 * 
 */

(function() {

  var parameters = PluginManager.parameters('Tonyryu_Alchimie');
  var param_dansMenu = Number(parameters['Dans menu'] || 1);
  var param_nomOption = String(parameters['Nom option'] || 'Alchimie');
  var param_affichage = Number(parameters['Affichage'] || 0);
  var param_achat_recettes = Number(parameters['AchatRecettes'] || 0);
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Party
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : initAllItems (surcharge)
  * Fonction : Ajoute la liste des recettes
  * Params : --
  **********************************************************************/
  var _TonyryuAlchimie_Game_Party_initAllItems = Game_Party.prototype.initAllItems;
  Game_Party.prototype.initAllItems = function() {
    _TonyryuAlchimie_Game_Party_initAllItems.call(this);
    this._listeRecetteAlchimie = {};
  };
  
  /**********************************************************************
  * Méthode : gainItem (surcharge)
  * Fonction : Ajoute ou retire une recette de la liste des recettes
  * Params : --
  **********************************************************************/
  var _TonyryuAlchimie_Game_Party_gainItem = Game_Party.prototype.gainItem;
  Game_Party.prototype.gainItem = function(item, amount, includeEquip) {
    _TonyryuAlchimie_Game_Party_gainItem.call(this, item, amount, includeEquip);
    if(item){
      if(item.note){
        var regExp = /\\AL\+\[(.*)\]/;
        var match = regExp.exec(item.note);
        if(match){
          if(amount > 0)
            this._listeRecetteAlchimie[match[1].toUpperCase()] = true;
          else
            this._listeRecetteAlchimie[match[1].toUpperCase()] = false;
        }
      }
    }
  };
  
  /**********************************************************************
  * Méthode : hasRecipe (surcharge)
  * Fonction : permet de savoir
  * Params : pRecette : nom de la recette
  **********************************************************************/
  Game_Party.prototype.hasRecipe = function(pRecette) {
    return !!this._listeRecetteAlchimie[pRecette];
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création d'une nouvelle "classe" Window_Tonyryu_Al_ItemCategory
  *  hérite de la classe Window_Command
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_Tonyryu_Al_ItemCategory() {
    this.initialize.apply(this, arguments);
  }

  Window_Tonyryu_Al_ItemCategory.prototype = Object.create(Window_Command.prototype);
  Window_Tonyryu_Al_ItemCategory.prototype.constructor = Window_Tonyryu_Al_ItemCategory;
  
  /**********************************************************************
  * Méthode : initialize
  * Fonction : Initialisation de la "classe" Window_Tonyryu_Al_ItemCategory
  * Params : pY : coordonnée Y de la fenêtre
  **********************************************************************/
  Window_Tonyryu_Al_ItemCategory.prototype.initialize = function(pY) {
    Window_Command.prototype.initialize.call(this, 0, pY);
    this._saveCateg;
  };
    
  /**********************************************************************
  * Méthode : windowWidth
  * Fonction : Retourne la largeur de la fenêtre
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemCategory.prototype.windowWidth = function() {
    return 300;
  };
  
  /**********************************************************************
  * Méthode : windowWidth
  * Fonction : Retourne la largeur de la fenêtre
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemCategory.prototype.numVisibleRows = function() {
    return this.maxItems();
  };
  
  /**********************************************************************
  * Méthode : makeCommandList
  * Fonction : Ajoute les commandes au menu
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemCategory.prototype.makeCommandList = function() {
    this.addCommand(TextManager.item, 'item');
    this.addCommand(TextManager.weapon, 'weapon');
    this.addCommand(TextManager.armor, 'armor');
    this.addCommand(TextManager.keyItem, 'keyItem');
  };
  
  /**********************************************************************
  * Méthode : setItemListWindow
  * Fonction : Permet de définir un llien avec la fenêtre listant les objets craftables
  * Params : pItemListWindow : instance de la fenêtre de liste des objets
  **********************************************************************/
  Window_Tonyryu_Al_ItemCategory.prototype.setItemListWindow = function(pItemListWindow) {
    this._itemListWindow = pItemListWindow;
  };

  /**********************************************************************
  * Méthode : update (surcharge)
  * Fonction : Appelé à chaque cycle, permet d'indiquer à la fenêtre de
  *            liste de recette, que la catégorie à changé 
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemCategory.prototype.update = function() {
    Window_Command.prototype.update.call(this);
    if(this._itemListWindow && this._saveCateg !== this._list[this._index].symbol){
      this._itemListWindow.setCategory(this._list[this._index].symbol);
      this._saveCateg = this._list[this._index].symbol;
    }
  };

  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création d'une nouvelle "classe" Window_Tonyryu_Al_ItemList
  *  hérite de la classe Window_Selectable
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_Tonyryu_Al_ItemList() {
    this.initialize.apply(this, arguments);
  }

  Window_Tonyryu_Al_ItemList.prototype = Object.create(Window_Selectable.prototype);
  Window_Tonyryu_Al_ItemList.prototype.constructor = Window_Tonyryu_Al_ItemList;
  
  /**********************************************************************
  * Méthode : initialize
  * Fonction : Initialisation de la "classe" Window_Tonyryu_Al_ItemList
  * Params : pY : coordonnée Y de la fenêtre
  *          pWidth : largeur de la fenêtre
  *          pHeight : hauteur de la fenêtre
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.initialize = function(pY, pWidth, pHeight) {
    Window_Selectable.prototype.initialize.call(this, 0, pY, pWidth, pHeight);
    this._category = 'none';
    this._data = [];
    this._save_index = 0;
    this._recapWindow = null;
  };
  
  /**********************************************************************
  * Méthode : maxCols (surcharge)
  * Fonction : Retourne le nombre maximum de colonne
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.maxCols = function() {
    return 1;
  };

  /**********************************************************************
  * Méthode : maxItems (surcharge)
  * Fonction : Retourne le nombre d'item maximum
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.maxItems = function() {
    return this._data ? this._data.length : 1;
  };
  
  /**********************************************************************
  * Méthode : setRecapWindow
  * Fonction : Permet de lier la fenêtre de récap
  * Params : pRecapWindow : objet de la fenêtre de recapitulatif
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.setRecapWindow = function(pRecapWindow) {
    this._recapWindow = pRecapWindow;
  };
  
  /**********************************************************************
  * Méthode : setCategory
  * Fonction : Permet de rafraichir la fenêtre si la catégorie à changé
  * Params : pCateg : nom de la catégorie
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.setCategory = function(pCateg) {
    this._category = pCateg;
    this.makeItemList();
    this.refresh();
  };
  
  /**********************************************************************
  * Méthode : makeItemList
  * Fonction : Construit la liste des objets craftable selon la catégorie
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.makeItemList = function() {
    this._data = [];
    var tabItem = [];
    var regExp = /\\AL\[(.*)\]/;
    var itemValid = true;
    var lCateg = '';
            
    if(this._category === 'item'){
      tabItem = $dataItems;
      lCateg = 'I';
    }else if(this._category === 'weapon'){
      tabItem = $dataWeapons;
      lCateg = 'W';
    }else if(this._category === 'armor'){
      tabItem = $dataArmors;
      lCateg = 'A';
    }else if(this._category === 'keyItem'){
      tabItem = $dataItems;
      lCateg = 'I';
    }
    
    for(var i = 1; i < tabItem.length; i++){
      itemValid = true;
      if(this._category === 'item'){
        if(tabItem[i].itypeId !== 1)
          itemValid = false;
      }
      if(this._category === 'keyItem'){
        if(tabItem[i].itypeId !== 2)
          itemValid = false;
      }        
      
      if(itemValid){
        if(tabItem[i]){
          if(param_achat_recettes === 0 || $gameParty.hasRecipe(lCateg + tabItem[i].id)){

            var match = regExp.exec(tabItem[i].note);
            if(match){
              var listeRecette = match[1];
              listeRecette = listeRecette.replace(' ', '');
              var tabRecette = listeRecette.split(';');
              for(var j = 0; j < tabRecette.length; j++){
                var tabRecipeFinish = [];
                var b_r_item = false;
                var b_valid = true;
                var listeIngredient = tabRecette[j].split(',');
                for(var k = 0; k < listeIngredient.length; k++){
                  var nbr = 0;
                  var id = 0;
                  var item = null;
                  var ingredient = listeIngredient[k].split('I');
                  if( ingredient.length === 2 ){
                    nbr = Number(ingredient[0]);
                    id = Number(ingredient[1]);
                    item = $dataItems[id];
                  }else{
                    ingredient = listeIngredient[k].split('W');
                    if( ingredient.length === 2 ){
                      nbr = Number(ingredient[0]);
                      id = Number(ingredient[1]);
                      item = $dataWeapons[id];
                    }else{
                      ingredient = listeIngredient[k].split('A');
                      if( ingredient.length === 2 ){
                        nbr = Number(ingredient[0]);
                        id = Number(ingredient[1]);
                        item = $dataArmors[id];
                      }
                    }
                  }
                  if(nbr > 0){
                    tabRecipeFinish.push({item:item, number:nbr}); 
                    var r_number = $gameParty.numItems(item);
                    if(param_affichage === 1)
                      b_r_item = true;
                    else if(param_affichage === 0 && r_number > 0)
                      b_r_item = true;

                    if(r_number < nbr)
                      b_valid = false;
                  }
                }
                if(b_r_item)
                  this._data.push({item:tabItem[i],valid:b_valid,tabRecipe:tabRecipeFinish});
              }
            }
          }
        }
      }
    }
  };
  
  /**********************************************************************
  * Méthode : refresh
  * Fonction : recré le contenu affichable
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.refresh = function() {
    this.createContents();
    this.drawAllItems();
  };
  
  /**********************************************************************
  * Méthode : drawItem
  * Fonction : dessine le nom d'un objet dans la liste
  * Params : index : index de l'objet dans le tableau
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.drawItem = function(index){
    var recipe = this._data[index];
    if(recipe){
      var rect = this.itemRect(index);
      rect.width -= 4;
      this.changePaintOpacity(recipe.valid);
      this.drawItemName(recipe.item, rect.x, rect.y, rect.width);
    }
  };
  
  /**********************************************************************
  * Méthode : getItemNumber
  * Fonction : Retourne le nombre d'objet craftable
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.getItemNumber = function() {
    return this._data.length;
  };
  
  /**********************************************************************
  * Méthode : updateHelp
  * Fonction : Met à jour la fenêtre d'aide avec l'objet sélectionné
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.updateHelp = function() {
    var index = this.index();
    if(this._helpWindow && index >= 0)
      this._helpWindow.setItem(this._data[index].item);
  };
  
  /**********************************************************************
  * Méthode : update (surcharge)
  * Fonction : Appelé à chaque cycle, permet d'indiquer à la fenêtre de
  *            recap, la recette choisie 
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.update = function() {
    Window_Selectable.prototype.update.call(this);
    var index = this.index();
    if(this._recapWindow && this._saveIndex !== index){
      this._recapWindow.setRecipe(this._data[index]);
      this._saveIndex = index;
    }
  };
  
  /**********************************************************************
  * Méthode : isCurrentItemEnabled
  * Fonction : retourne vrai si l'objet est actif 
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.isCurrentItemEnabled = function() {
    var index = this.index();
    return this._data[index].valid;
  };
  
  /**********************************************************************
  * Méthode : getCurrentRecipe
  * Fonction : retourne l'objet sélectionné
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.getCurrentRecipe = function() {
    var index = this.index();
    return this._data[index];
  };
  
  
  /**********************************************************************
  * Méthode : updateData
  * Fonction : met à jour la liste des recettes
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_ItemList.prototype.updateData = function() {
    for(var i = 0; i < this._data.length; i++){
      var valid = true;
      for(var j = 0; j < this._data[i].tabRecipe.length; j++){
        var p_number = $gameParty.numItems(this._data[i].tabRecipe[j].item);
        if(p_number < this._data[i].tabRecipe[j].number)
          valid = false;
      }
      this._data[i].valid = valid;
    }
    this.refresh();
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création d'une nouvelle "classe" Window_Tonyryu_Al_Recap
  *  hérite de la classe Window_Base
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_Tonyryu_Al_Recap() {
    this.initialize.apply(this, arguments);
  }

  Window_Tonyryu_Al_Recap.prototype = Object.create(Window_Base.prototype);
  Window_Tonyryu_Al_Recap.prototype.constructor = Window_Tonyryu_Al_Recap;
  
  /**********************************************************************
  * Méthode : initialize
  * Fonction : Initialisation de la "classe" Window_Tonyryu_Al_Recap
  * Params : pX : coordonnée X de la fenêtre
  *          pY : coordonnée Y de la fenêtre
  *          pWidth : largeur de la fenêtre
  *          pHeight : hauteur de la fenêtre
  **********************************************************************/
  Window_Tonyryu_Al_Recap.prototype.initialize = function(pX, pY, pWidth, pHeight) {
    Window_Base.prototype.initialize.call(this, pX, pY, pWidth, pHeight);
    this._recipe = null;
  };
  
  /**********************************************************************
  * Méthode : setRecipe
  * Fonction : Postionne l'objet de la liste afin d'avoir ces recettes
  * Params : pItem : objet 
  **********************************************************************/
  Window_Tonyryu_Al_Recap.prototype.setRecipe = function(pRecipe) {
    if(pRecipe){
      this._recipe = pRecipe;
      this.refresh();
    }
  };
  
  /**********************************************************************
  * Méthode : refresh
  * Fonction : recrée le contenu de la fenêtre
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Al_Recap.prototype.refresh = function() {
    this.clear(false);
    var item = this._recipe.item;
    
    this.resetTextColor();
    var y = 10;
    var x = 4;
    this.drawParam(x, y, 2, item);
    this.drawParam(x, y + this.lineHeight(), 3, item);
    this.drawParam(x, y + (this.lineHeight() * 2), 4, item);
    x = 8 + (this.contents.width / 2);
    this.drawParam(x, y, 5, item);
    this.drawParam(x, y + this.lineHeight(), 6, item);
    this.drawParam(x, y + (this.lineHeight() * 2), 7, item);
    
    y = y + (this.lineHeight() * 3) + 16;
    this.contents.fillRect(0, y, this.contents.width, 2, this.normalColor());
    
    x = 30;
    y += 16;
    
    for(var i = 0; i < this._recipe.tabRecipe.length; i++){
      var i_item = this._recipe.tabRecipe[i].item;
      var i_number = this._recipe.tabRecipe[i].number;
      this.drawIngredient(x, y, i_item, i_number);
      y += this.lineHeight();
    }
  };
  
  /**********************************************************************
  * Méthode : clear
  * Fonction : Efface le contenu de la fenêtre
  * Params : pForce : forçage de la suppression de l'objet 
  **********************************************************************/
  Window_Tonyryu_Al_Recap.prototype.clear = function(pForce) {
    this.contents.clear();
    if(pForce)
      this._item = null;
  };
  
  /**********************************************************************
  * Méthode : drawParam
  * Fonction : Affiche un paramètre d'équipement
  * Params : pX : Coordonnée X de la zone d'écriture
  *          pY : Coordonnée Y de la zone d'écriture
  *          pParamId : Id du paramètre à écrire
  *          pItem : Objet traité
  **********************************************************************/
  Window_Tonyryu_Al_Recap.prototype.drawParam = function(pX, pY, pParamId, pItem) {
    this.changeTextColor(this.systemColor());
    this.drawText(TextManager.param(pParamId), pX, pY, 140);
    this.resetTextColor();
    if(pItem.params){
      this.drawText(String(pItem.params[pParamId]), pX + 160, pY, 140);
    }
  };

  /**********************************************************************
  * Méthode : drawIngredient
  * Fonction : Affiche un ingrédient
  * Params : pX : Coordonnée X de la zone d'écriture
  *          pY : Coordonnée Y de la zone d'écriture
  *          pItem : Objet traité
  *          pNumber : Nombre nécessaire
  **********************************************************************/
  Window_Tonyryu_Al_Recap.prototype.drawIngredient = function(pX, pY, pItem, pNumber) {
    var p_number = $gameParty.numItems(pItem);
    this.changePaintOpacity(p_number >= pNumber);
    this.drawText(p_number + "/" + pNumber, pX, pY, 100);
    this.drawItemName(pItem, pX + 60, pY, 400);
    this.changePaintOpacity(true);
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création d'une nouvelle "classe" Scene_Tonyryu_Alchimie
  *  hérite de la classe Scene_MenuBase
  *----------------------------------------------------------------------
  **********************************************************************/
  function Scene_Tonyryu_Alchimie() {
    this.initialize.apply(this, arguments);
  }

  Scene_Tonyryu_Alchimie.prototype = Object.create(Scene_MenuBase.prototype);
  Scene_Tonyryu_Alchimie.prototype.constructor = Scene_Tonyryu_Alchimie;

  /**********************************************************************
  * Méthode : initialize
  * Fonction : Initialisation de la "classe" Scene_Tonyryu_Alchimie
  * Params : --
  **********************************************************************/
  Scene_Tonyryu_Alchimie.prototype.initialize = function() {
    Scene_MenuBase.prototype.initialize.call(this);
  };
  
  /**********************************************************************
  * Méthode : create (surcharge)
  * Fonction : Permet d'appeler les fonctions de construction de la scene
  * Params : --
  **********************************************************************/
  Scene_Tonyryu_Alchimie.prototype.create = function() {
    Scene_MenuBase.prototype.create.call(this);
    this.createHelpWindow();
    this.createItemCategWindow();
    this.createItemListWindow();
    this.createRecapWindow();
  };
  
  /**********************************************************************
  * Méthode : createItemCategWindow
  * Fonction : Permet d'ajouter la fenêtre de catégorie d'objet
  * Params : --
  **********************************************************************/
  Scene_Tonyryu_Alchimie.prototype.createItemCategWindow = function() {
    var wy = this._helpWindow.height;
    this._Al_ItemCateg_Window = new Window_Tonyryu_Al_ItemCategory(wy);
    this._Al_ItemCateg_Window.setHandler('ok',     this.onCategoryOk.bind(this));
    this._Al_ItemCateg_Window.setHandler('cancel', this.popScene.bind(this));
    this.addWindow(this._Al_ItemCateg_Window);
  };
  
  /**********************************************************************
  * Méthode : createItemListWindow
  * Fonction : Permet d'ajouter la fenêtre de liste d'objets craftables
  * Params : --
  **********************************************************************/
  Scene_Tonyryu_Alchimie.prototype.createItemListWindow = function() {
    var wy = this._Al_ItemCateg_Window.y + this._Al_ItemCateg_Window.height;
    var ww = this._Al_ItemCateg_Window.width;
    var wh = Graphics.boxHeight - wy;
    this._Al_ItemList_Window = new Window_Tonyryu_Al_ItemList(wy, ww, wh);
    this._Al_ItemList_Window.setHandler('ok',     this.onItemOk.bind(this));
    this._Al_ItemList_Window.setHandler('cancel', this.onItemCancel.bind(this));
    this._Al_ItemList_Window.setHelpWindow(this._helpWindow);
    this._Al_ItemCateg_Window.setItemListWindow(this._Al_ItemList_Window);
    this.addWindow(this._Al_ItemList_Window);
  };
  
  
  /**********************************************************************
  * Méthode : createRecapWindow
  * Fonction : Permet d'ajouter la fenêtre de récapitulatif
  * Params : --
  **********************************************************************/
  Scene_Tonyryu_Alchimie.prototype.createRecapWindow = function() {
    var wx = this._Al_ItemList_Window.width;
    var wy = this._helpWindow.height;
    var ww = Graphics.boxWidth - this._Al_ItemCateg_Window.width;
    var wh = Graphics.boxHeight - wy;
    this._Al_Recap_Window = new Window_Tonyryu_Al_Recap(wx, wy, ww, wh);
    this._Al_ItemList_Window.setRecapWindow(this._Al_Recap_Window);
    this.addWindow(this._Al_Recap_Window);
  };
  
  /**********************************************************************
  * Méthode : onCategoryOk
  * Fonction : Permet de basculer l'activité sur la fenêtre de liste
  * Params : --
  **********************************************************************/
  Scene_Tonyryu_Alchimie.prototype.onCategoryOk = function() {
    if(this._Al_ItemList_Window.getItemNumber() > 0){
      this._Al_ItemList_Window.activate();
      this._Al_ItemList_Window.select(0);
    }else
      this._Al_ItemCateg_Window.activate();
  };
  
  /**********************************************************************
  * Méthode : onItemOk
  * Fonction : Permet de crafter l'objet
  * Params : --
  **********************************************************************/
  Scene_Tonyryu_Alchimie.prototype.onItemOk = function() {
    var recipe = this._Al_ItemList_Window.getCurrentRecipe();
    
    $gameParty.gainItem(recipe.item, 1);
    for(var i = 0; i < recipe.tabRecipe.length; i++){
      var i_item = recipe.tabRecipe[i].item;
      var i_number = recipe.tabRecipe[i].number;
      $gameParty.gainItem(i_item, -i_number);
    }
    this._Al_ItemList_Window.updateData();
    this._Al_Recap_Window.refresh();
    this._Al_ItemList_Window.activate();
  };
  
  /**********************************************************************
  * Méthode : createItemCategWindow
  * Fonction : Permet d'ajouter la fenêtre de catégorie d'objet
  * Params : --
  **********************************************************************/
  Scene_Tonyryu_Alchimie.prototype.onItemCancel = function() {
    this._Al_ItemList_Window.deselect();
    this._Al_ItemCateg_Window.activate();
    this._helpWindow.clear();
    this._Al_Recap_Window.clear(true);
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Window_MenuCommand
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : addMainCommands (surcharge)
  * Fonction : Ajoute l'option d'alchimie de le menu
  * Params : --
  **********************************************************************/
  var _Window_MenuCommand_addMainCommands = Window_MenuCommand.prototype.addMainCommands;
  Window_MenuCommand.prototype.addMainCommands = function() {
    _Window_MenuCommand_addMainCommands.call(this);
    
    if(param_dansMenu === 1){
      var enabled = this.areMainCommandsEnabled();
      if (this.needsCommand('alchimie')) {
        this.addCommand(param_nomOption, 'alchimie', enabled);
      }
    }
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Scene_Menu
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : createCommandWindow (surcharge)
  * Fonction : Ajoute l'handle sur la commande alchimie
  * Params : --
  **********************************************************************/
  var _Scene_Menu_createCommandWindow = Scene_Menu.prototype.createCommandWindow;
  Scene_Menu.prototype.createCommandWindow = function(){
    _Scene_Menu_createCommandWindow.call(this);
    if(param_dansMenu === 1)
      this._commandWindow.setHandler('alchimie', this.commandAlchimie.bind(this));
  };
  
  /**********************************************************************
  * Méthode : commandAlchimie (surcharge)
  * Fonction : Permet de passer sur la scene de gestion d'alchimie
  * Params : --
  **********************************************************************/
  Scene_Menu.prototype.commandAlchimie = function() {
    SceneManager.push(Scene_Tonyryu_Alchimie);
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Interpreter
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : pluginCommand (surcharge)
  * Fonction : Ajoute la commande TonyryuAlchimie
  * Params : --
  **********************************************************************/
  var _Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
  Game_Interpreter.prototype.pluginCommand = function(command, args) {
    _Game_Interpreter_pluginCommand.call(this, command, args);
    if(command === 'TonyryuAlchimie')
      SceneManager.push(Scene_Tonyryu_Alchimie);
    else if(command === 'TonyryuAlchimieAdd')
      $gameParty._listeRecetteAlchimie[args[0]] = true;
    else if(command === 'TonyryuAlchimieDel')
      $gameParty._listeRecetteAlchimie[args[0]] = false;
  };
  
})();
