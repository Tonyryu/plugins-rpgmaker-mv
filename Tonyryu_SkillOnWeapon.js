﻿//=============================================================================
// Tonyryu_SkillOnWeapon.js
//=============================================================================

/*:
 * @plugindesc Plugin permettant d'apprendre des compétences via les armes. (1.02)
 * @author Tonyryu
 *
 * @param CP=PourcentXP
 * @desc Pourcentage d'exp converti en CP, si non configuré sur ennemi
 * @default 10
 *
 * @param Mastered Text
 * @desc Texte affiché sous la jauge quant compétence acquise
 * @default Acquise
 * 
 * @param Cp Color Bar 1
 * @desc Couleur 1 du dégradé de la barre de progression de CP
 * @default #0000cc
 *
 * @param Cp Color Bar 2
 * @desc Couleur 2 du dégradé de la barre de progression de CP
 * @default #0000ff
 * 
 * @param Cp Color Frame
 * @desc Couleur de l'encadré de progression de CP
 * @default #ffffff
 * 
 * @param Cp * Nbr equip skill
 * @desc 1 : CP gagnés augentent si plusieurs fois le même skill équippé
 * @default 0
 *
 * @help http://www.tonyryudev.com/
 * 
 * - Ajouter les compétences qui peuvent être apprise sur la liste des
 *   compétences d'une classe
 * - Ajouter un tag \CP[] sur les compétences (Remarque), afin de définir le
 *   nombre de points de compétences nécessaire pour les apprendre. ex : \CP[100]
 * - Ajouter un tag \CP[] sur les équipements (Remarque), afin de définir la liste
 *   des compétences de l'arme via leurs ID. ex \CP[4,9] Compétence ID 0004 et 0009
 * - Ajouter un tag \CP[] sur les ennemis (Remarque), afin de définir un nombre de
 *   points de compétences gagnés en fin de combat. ex : \CP[10]. Si pas défini,
 *   c'est le pourcentage en paramètre qui est utilisé
 * 
 */

/*
 * Suivi de version 
 * 1.00 : 11/11/2015 : Tonyryu : Création plugin
 * 1.01 : 11/11/2015 : Tonyryu : Correction bug d'affichage dans liste des skills
 * 1.02 : 15/11/2015 : Tonyryu : Correction couleur grisé skill sur équipement
 */

(function() {

  var parameters = PluginManager.parameters('Tonyryu_SkillOnWeapon');
  
  var param_pourcentXP = Number(parameters['CP=PourcentXP'] || 10);
  var param_textMastered = String(parameters['Mastered Text'] || 'Acquise');
  var param_cpBarColor1 = String(parameters['Cp Color Bar 1'] || '#0000cc');
  var param_cpBarColor2 = String(parameters['Cp Color Bar 2'] || '#0000cc');
  var param_cpColorFrame = String(parameters['Cp Color Frame'] || '#ffffff');
  var param_optMultiplicateur = Number(parameters['Cp * Nbr equip skill'] || 0);

  /**********************************************************************
  * Méthode : getListSkills
  * Fonction : Retourne un tableau d'ID de compétences d'un objet
  * Params : --
  **********************************************************************/
  getListSkills = function(pItem) {
    var regExp = /\\CP\[(.*)\]/;
    var tabComp = [];
    var match = regExp.exec(pItem.note);
    if(match){
      var listeComp = match[1];
      listeComp = listeComp.replace(' ', '');
      tabComp = listeComp.split(',');
    }
    for(var i = 0; i < tabComp.length; i++)
      tabComp[i] = Number(tabComp[i]);
    return tabComp;
  };

  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Actor
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : levelUp (remplacement)
  * Fonction : Retire la possibilité de gagner des skill en lvl up
  * Params : --
  **********************************************************************/
  Game_Actor.prototype.levelUp = function() {
    this._level++;
  };

  /**********************************************************************
  * Méthode : initSkills (surcharge)
  * Fonction : Retire les compétences ajoutés lors de la création du jeu
  * Params : --
  **********************************************************************/
  Game_Actor.prototype.initSkills = function() {
    this._skills = [];
    this._skillsLearn = [];
  };
  
  /**********************************************************************
  * Méthode : listSkillEquip
  * Fonction : Retourne un tableau de JSON contenant toutes les compétences équipées
  * Params : --
  **********************************************************************/
  Game_Actor.prototype.listSkillEquip = function(){
    var tabSkillTotal = [];
    
    for (var i = 0; i < this._equips.length; i++) {
      if(this._equips[i]){
        if(this._equips[i]._itemId > 0){
          var tabSkill = [];
          if(this._equips[i]._dataClass === 'weapon')
            tabSkill = tabSkill.concat(getListSkills($dataWeapons[this._equips[i]._itemId]));
          if(this._equips[i]._dataClass === 'armor')
            tabSkill = tabSkill.concat(getListSkills($dataArmors[this._equips[i]._itemId]));
          
          for (var j = 0; j < tabSkill.length; j++){
            var idx = tabSkillTotal.indexOf(tabSkill[j]);
            if(idx === -1)
              tabSkillTotal.push({_idSkill:tabSkill[j], _nbSkill:1});
            else
              tabSkillTotal[idx]._nbSkill ++; 
          }
        }
      }
    }
    return tabSkillTotal;
  };
  
  /**********************************************************************
  * Méthode : listSkillEquip
  * Fonction : Retourne un tableau de JSON contenant toutes les compétences équipées
  * Params : --
  **********************************************************************/
  Game_Actor.prototype.redefineSkills = function(){
    this._skills = [];
    for (var i = 0; i < this._skillsLearn.length; i++) {
      if(this.skillMastered(this._skillsLearn[i]._skillId))
        this.learnSkill(this._skillsLearn[i]._skillId);
    }
    
    var tabSkill = this.listSkillEquip();
    for (var i = 0; i < tabSkill.length; i++) {
      if(this.skillPossible(tabSkill[i]._idSkill))
        this.learnSkill(tabSkill[i]._idSkill);
    }
  };

  /**********************************************************************
  * Méthode : initEquips (surcharge)
  * Fonction : Permet de redéfinir la liste des skills du personnage en
  *              fonction de son equipement de départ
  * Params : --
  **********************************************************************/
  var _Tonyryu_SoW_Game_Actor_initEquips = Game_Actor.prototype.initEquips;
  Game_Actor.prototype.initEquips = function(equips) {
    _Tonyryu_SoW_Game_Actor_initEquips.call(this, equips);
    this.redefineSkills();
  };
  
  /**********************************************************************
  * Méthode : changeEquip (surcharge)
  * Fonction : Permet de redéfinir la liste des skills du personnage lors
  *             d'un changement d'équipement
  * Params : --
  **********************************************************************/
  var _Tonyryu_SoW_Game_Actor_changeEquip = Game_Actor.prototype.changeEquip;
  Game_Actor.prototype.changeEquip = function(slotId, item) {
    _Tonyryu_SoW_Game_Actor_changeEquip.call(this, slotId, item);
    this.redefineSkills();
  };

  /**********************************************************************
  * Méthode : forceChangeEquip (surcharge)
  * Fonction : Permet de redéfinir la liste des skills du personnage lors
  *             d'un changement d'équipement forcé
  * Params : --
  **********************************************************************/
  var _Tonyryu_SoW_Game_Actor_forceChangeEquip = Game_Actor.prototype.forceChangeEquip;
  Game_Actor.prototype.forceChangeEquip = function(slotId, item) {
    _Tonyryu_SoW_Game_Actor_forceChangeEquip.call(this, slotId, item);
    this.redefineSkills();
  };

  /**********************************************************************
  * Méthode : cpSkill
  * Fonction : Connaitre le nombre de cp acquis d'une compétence
  * Params : pIdSkill : id de la compétence
  **********************************************************************/
  Game_Actor.prototype.cpSkill = function(pIdSkill) {
    for(var i = 0; i < this._skillsLearn.length; i++){
      if(this._skillsLearn[i]._skillId === pIdSkill)
        return this._skillsLearn[i]._cp;
    }
    return 0;
  };
  
  /**********************************************************************
  * Méthode : cpSkillNeeded
  * Fonction : Connaitre le nombre de cp acquis d'une compétence
  * Params : pIdSkill : id de la compétence
  **********************************************************************/
  Game_Actor.prototype.cpSkillNeeded = function(pIdSkill) {
    for(var i = 0; i < this._skillsLearn.length; i++){
      if(this._skillsLearn[i]._skillId === pIdSkill)
        return this._skillsLearn[i]._cpNeeded;
    }
    var regExp = /\\CP\[(.*)\]/;
    var match = regExp.exec($dataSkills[pIdSkill].note);
    var cpNeeded = 0;
    if(match){
      var stringCpNeeded = match[1];
      cpNeeded = Number(stringCpNeeded.replace(' ', ''));
    }
    return cpNeeded;
  };
  
  /**********************************************************************
  * Méthode : skillMastered
  * Fonction : Permet de savoir si une compétence est maitrisé
  * Params : pIdSkill : id de la compétence
  **********************************************************************/
  Game_Actor.prototype.skillMastered = function(pIdSkill) {
    for(var i = 0; i < this._skillsLearn.length; i++){
      if(this._skillsLearn[i]._skillId === pIdSkill)
        return this._skillsLearn[i]._cp === this._skillsLearn[i]._cpNeeded;
    }
    return false;
  };
  
  /**********************************************************************
  * Méthode : skillsMastered
  * Fonction : Permet de savoir si un skill est maitrisé
  * Params : pIdSkill : id de la compétence
  **********************************************************************/
  Game_Actor.prototype.skillCpRate = function(pIdSkill) {
    for(var i = 0; i < this._skillsLearn.length; i++){
      if(this._skillsLearn[i]._skillId === pIdSkill)
        return this._skillsLearn[i]._cp / this._skillsLearn[i]._cpNeeded;
    }
    return 0;
  };

  /**********************************************************************
  * Méthode : skillPossible
  * Fonction : Retourne un booleen permettant de savoir si le player peut gagner cette compétence
  * Params : pIdSkill : id de la compétence
  **********************************************************************/
  Game_Actor.prototype.skillPossible = function(pIdSkill) {
    for(var i = 0; i < $dataClasses[this._classId].learnings.length; i++){
      if($dataClasses[this._classId].learnings[i].skillId === pIdSkill)
        return true;
    }
    return false;
  };
  
  /**********************************************************************
  * Méthode : learnSkill (surcharge)
  * Fonction : Permet d'ajouter la compétence dans la liste en cours d'aprentissage
  * Params : pIdSkill : id de la compétence
  **********************************************************************/
  var _Tonyryu_SoW_Game_Actor_learnSkill = Game_Actor.prototype.learnSkill;
  Game_Actor.prototype.learnSkill = function(skillId) {
    _Tonyryu_SoW_Game_Actor_learnSkill.call(this, skillId);
    var skillFind = -1;
    for(var i = 0; i < this._skillsLearn.length; i++){
      if(this._skillsLearn[i]._skillId === skillId){
        skillFind = i;
        break;
      }
    }
    if(skillFind === -1){
      this._skillsLearn[this._skillsLearn.length] = {_skillId:skillId, _cp:0, _cpNeeded:this.cpSkillNeeded(skillId)};
    }
  };
  
  /**********************************************************************
  * Méthode : gainCp
  * Fonction : Ajoute des point de compétence sur les compétences du personnage
  * Params : cp : nombre de point à ajouter
  **********************************************************************/
  Game_Actor.prototype.gainCp = function(cp) {
    var tabSkill = this.listSkillEquip();
    var newPage = false;
    for (var i = 0; i < tabSkill.length; i++) {
      for (var j = 0; j < this._skillsLearn.length; j++) {
        if(this._skillsLearn[j]._skillId === tabSkill[i]._idSkill){
          if(this._skillsLearn[j]._cp !== this._skillsLearn[j]._cpNeeded){
            if(param_optMultiplicateur === 1)
              cp = cp * tabSkill[i]._nbSkill;
            this._skillsLearn[j]._cp = Math.min(this._skillsLearn[j]._cp + cp, this._skillsLearn[j]._cpNeeded);
            if(this._skillsLearn[j]._cp === this._skillsLearn[j]._cpNeeded){
              if(!newPage){
                $gameMessage.newPage();
                newPage = true;
              }
              $gameMessage.add(TextManager.skill + ' [' + $dataSkills[this._skillsLearn[j]._skillId].name + '] ' + param_textMastered);
            }
          }
        }
      }
    }
  };

  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Enemy
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : cp
  * Fonction : Retourne le nombre de point de compétence du monstre
  * Params : --
  **********************************************************************/
  Game_Enemy.prototype.cp = function() {
    var regExp = /\\CP\[(.*)\]/;
    var match = regExp.exec($dataEnemies[this._enemyId].note);
    var cp = 0;
    if(match){
      var stringCp = match[1];
      cp = Number(stringCp.replace(' ', ''));
    }
    if(cp === 0)
      cp = Math.floor((this.exp() * param_pourcentXP)/ 100);
    return cp;
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Troop
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : cpTotal
  * Fonction : Retourne le nombre de point de compétence de la troupe
  * Params : --
  **********************************************************************/
  Game_Troop.prototype.cpTotal = function() {
    return this.deadMembers().reduce(function(r, enemy) {
      return r + enemy.cp();
    }, 0);
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification du "module" BattleManager
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : makeRewards (Surcharge)
  * Fonction : Ajout du nombre de point de compétence total au gain
  * Params : --
  **********************************************************************/ 
  var _Tonyryu_SoW_BattleManager_makeRewards = BattleManager.makeRewards;
  BattleManager.makeRewards = function() {
    _Tonyryu_SoW_BattleManager_makeRewards.call(this);
    this._rewards.cp = $gameTroop.cpTotal();
  };
  
  /**********************************************************************
  * Méthode : displayExp (Surcharge)
  * Fonction : Ajout de l'affichage des points de compétences gagnés
  * Params : --
  **********************************************************************/ 
  var _Tonyryu_SoW_BattleManager_displayExp = BattleManager.displayExp;
  BattleManager.displayExp = function() {
    _Tonyryu_SoW_BattleManager_displayExp.call(this);
    var cp = this._rewards.cp;
    if (cp > 0) {
      var text = TextManager.obtainExp.format(cp, 'CP');
      $gameMessage.add('\\.' + text);
    }
  };
  
  /**********************************************************************
  * Méthode : gainExp (Surcharge)
  * Fonction : Ajout du gain des points de compétences gagnés
  * Params : --
  **********************************************************************/ 
  var _Tonyryu_SoW_BattleManager_gainExp = BattleManager.gainExp;
  BattleManager.gainExp = function() {
    _Tonyryu_SoW_BattleManager_gainExp.call(this);
    this.gainCp();
  };

  /**********************************************************************
  * Méthode : gainCp
  * Fonction : affecte le gain de CP a chaque membre du groupe
  * Params : --
  **********************************************************************/ 
  BattleManager.gainCp = function() {
    var cp = this._rewards.cp;
    $gameParty.allMembers().forEach(function(actor) {
       actor.gainCp(cp);
    });
  };
  

  /**********************************************************************
  *----------------------------------------------------------------------
  * Création d'une nouvelle "classe" Window_Tonyryu_SoW_Equip
  *  hérite de la classe Window_Base
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_Tonyryu_SoW_Equip() {
    this.initialize.apply(this, arguments);
  }

  Window_Tonyryu_SoW_Equip.prototype = Object.create(Window_Base.prototype);
  Window_Tonyryu_SoW_Equip.prototype.constructor = Window_Tonyryu_SoW_Equip;
  
  /**********************************************************************
  * Méthode : initialize
  * Fonction : Initialisation de la "classe" Window_Tonyryu_Al_Recap
  * Params : pX : coordonnée X de la fenêtre
  *          pY : coordonnée Y de la fenêtre
  *          pWidth : largeur de la fenêtre
  *          pHeight : hauteur de la fenêtre
  **********************************************************************/
  Window_Tonyryu_SoW_Equip.prototype.initialize = function(pX, pY, pWidth, pHeight) {
    Window_Base.prototype.initialize.call(this, pX, pY, pWidth, pHeight);
    this._item = null;
    this._actor = null;
  };
  
  /**********************************************************************
  * Méthode : refresh
  * Fonction : recréer l'affichage de la fenêtre
  * Params : --
  **********************************************************************/ 
  Window_Tonyryu_SoW_Equip.prototype.refresh = function(){
    var save_fontsize = this.contents.fontSize;
    this.contents.clear();
    this.changeTextColor(this.systemColor());
    this.changePaintOpacity(true);
    this.contents.drawText(TextManager.skill , 0, 0, this.contents.width, this.lineHeight());
    this.changeTextColor(this.normalColor());
    if(this._item !== null & this._actor !== null){
      var tabSkills = getListSkills(this._item);
      for( var i = 0; i < tabSkills.length; i ++){
        this.changePaintOpacity(this._actor.skillPossible(tabSkills[i]));
        this.contents.drawText($dataSkills[tabSkills[i]].name, 0, this.lineHeight() * (i+1), this.contents.width, this.lineHeight());
        
        if(this._actor.skillPossible(tabSkills[i])){
          this.contents.fillRect(this.contents.width - 100, this.lineHeight() * (i+1) + 10, 1, 20, param_cpColorFrame);
          this.contents.fillRect(this.contents.width - 100, this.lineHeight() * (i+1) + 30 - 1, 100, 1, param_cpColorFrame);
          this.contents.fillRect(this.contents.width - 100, this.lineHeight() * (i+1) + 10, 100, 1, param_cpColorFrame);
          this.contents.fillRect(this.contents.width - 1, this.lineHeight() * (i+1) + 10, 1, 20, param_cpColorFrame);
          this.contents.gradientFillRect(this.contents.width - 99, this.lineHeight() * (i+1) + 11, 98 * this._actor.skillCpRate(tabSkills[i]), 18, param_cpBarColor1, param_cpBarColor2);
          this.contents.fontSize = 16;
          var text = String(this._actor.cpSkill(tabSkills[i])) + "/" + String(this._actor.cpSkillNeeded(tabSkills[i]));
          if(this._actor.skillMastered(tabSkills[i]))
            text = param_textMastered;
          this.contents.drawText(text, this.contents.width - 90, this.lineHeight() * (i+1) + 20, 70, 20);
          this.contents.fontSize = save_fontsize ;
        }
      }
    }
  };

  /**********************************************************************
  * Méthode : setActor
  * Fonction : Permet d'affecter l'acteur à la fenêtre
  * Params : pActor : Acteur géré dans la scene d'équipement
  **********************************************************************/ 
  Window_Tonyryu_SoW_Equip.prototype.setActor = function(pActor){
    if (this._actor !== pActor) {
      this._actor = pActor;
      this.refresh();
    }
  };
  
  /**********************************************************************
  * Méthode : setItem
  * Fonction : Permet de mettre à jour l'affichage avec les données d'un objet
  * Params : pItem : objet utilisé pour la mise à jour
  **********************************************************************/ 
  Window_Tonyryu_SoW_Equip.prototype.setItem = function(pItem){
    if(pItem)
      this._item = pItem;
    else
      this._item = null;
    this.refresh();
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification du "classe" Window_EquipItem
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : maxCols (Surcharge)
  * Fonction : Défini le nombre maximum de colonne
  * Params : --
  **********************************************************************/ 
  Window_EquipItem.prototype.maxCols = function() {
    return 1;
  };
  
  /**********************************************************************
  * Méthode : setSowEquipWindow
  * Fonction : Réalise le lien avec la fenêtre des compétence d'équipement
  * Params : sowEquipWindow : Fenêtre affichant la liste des compétences
  **********************************************************************/ 
  Window_EquipItem.prototype.setSowEquipWindow = function(sowEquipWindow) {
    this._sowEquipWindow = sowEquipWindow;
    this.callUpdateHelp();
  };
  
  /**********************************************************************
  * Méthode : updateHelp (surcharge)
  * Fonction : Ajoute la transmission de l'objet vers la fenêtre listant les compétences
  * Params : --
  **********************************************************************/ 
  var _Tonyryu_SoW_Window_EquipItem_updateHelp = Window_EquipItem.prototype.updateHelp;
  Window_EquipItem.prototype.updateHelp = function() {
    _Tonyryu_SoW_Window_EquipItem_updateHelp.call(this);
    if (this._sowEquipWindow) {
      this._sowEquipWindow.setItem(this.item());
    }
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification du "classe" Window_EquipSlot
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : setSowEquipWindow
  * Fonction : Réalise le lien avec la fenêtre des compétence d'équipement
  * Params : sowEquipWindow : Fenêtre affichant la liste des compétences
  **********************************************************************/ 
  Window_EquipSlot.prototype.setSowEquipWindow = function(sowEquipWindow) {
    this._sowEquipWindow = sowEquipWindow;
    this.callUpdateHelp();
  };

  /**********************************************************************
  * Méthode : updateHelp (Surcharge)
  * Fonction : Ajoute la transmission de l'objet vers la fenêtre listant les compétences
  * Params : --
  **********************************************************************/ 
  var _Tonyryu_SoW_Window_EquipSlot_updateHelp = Window_EquipSlot.prototype.updateHelp;
  Window_EquipSlot.prototype.updateHelp = function() {
    _Tonyryu_SoW_Window_EquipSlot_updateHelp.call(this);
    if (this._sowEquipWindow) {
      this._sowEquipWindow.setItem(this.item());
    }
  };

  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification du "classe" Scene_Equip
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : createItemWindow (Remplacement)
  * Fonction : Création de la fenêtre listant les équipements
  * Params : --
  **********************************************************************/ 
  Scene_Equip.prototype.createItemWindow = function() {
    var wx = Graphics.boxWidth / 2;
    var wy = this._statusWindow.y + this._statusWindow.height;
    var ww = Graphics.boxWidth - wx;
    var wh = Graphics.boxHeight -  wy;
    this._itemWindow = new Window_EquipItem(wx, wy, ww, wh);
    this._itemWindow.setHelpWindow(this._helpWindow);
    this._itemWindow.setStatusWindow(this._statusWindow);
    this._itemWindow.setHandler('ok',     this.onItemOk.bind(this));
    this._itemWindow.setHandler('cancel', this.onItemCancel.bind(this));
    this._slotWindow.setItemWindow(this._itemWindow);
    this.addWindow(this._itemWindow);
  };

  /**********************************************************************
  * Méthode : createStatusWindow (Surcharge)
  * Fonction : Ajoute la création de la fenêtre listant les compétences
  * Params : --
  **********************************************************************/ 
  var _Tonyryu_SoW_Scene_Equip_createStatusWindow = Scene_Equip.prototype.createStatusWindow;
  Scene_Equip.prototype.createStatusWindow = function() {
    _Tonyryu_SoW_Scene_Equip_createStatusWindow.call(this);
    this.createSowEquipWindow();
  };
  
  /**********************************************************************
  * Méthode : createSowEquipWindow
  * Fonction : Permet de créer la fenêtre listant les compétences
  * Params : --
  **********************************************************************/ 
  Scene_Equip.prototype.createSowEquipWindow = function() {
    var wx = 0;
    var wy = this._statusWindow.y + this._statusWindow.height;
    var ww = Graphics.boxWidth / 2;
    var wh = Graphics.boxHeight - wy;
    this._sowEquipWindow = new Window_Tonyryu_SoW_Equip(wx, wy, ww, wh);
    this.addWindow(this._sowEquipWindow);
  };

  /**********************************************************************
  * Méthode : createSlotWindow (Surcharge)
  * Fonction : Ajoute le lien avec la fenêtre des compétences sur les slots
  * Params : --
  **********************************************************************/
  var _Tonyryu_SoW_Scene_Equip_createSlotWindow = Scene_Equip.prototype.createSlotWindow;
  Scene_Equip.prototype.createSlotWindow = function() {
    _Tonyryu_SoW_Scene_Equip_createSlotWindow.call(this);
    this._slotWindow.setSowEquipWindow(this._sowEquipWindow);
  };
  
  /**********************************************************************
  * Méthode : refreshActor (Surcharge)
  * Fonction : Envoi l'acteur à la fenêtre listant les compétences
  * Params : --
  **********************************************************************/
  var _Tonyryu_SoW_Scene_Equip_refreshActor = Scene_Equip.prototype.refreshActor;
  Scene_Equip.prototype.refreshActor = function() {
    _Tonyryu_SoW_Scene_Equip_refreshActor.call(this);
    var actor = this.actor();
    this._sowEquipWindow.setActor(actor);
  };

  /**********************************************************************
  * Méthode : createItemWindow
  * Fonction : Ajoute le lien avec la fenêtre des compétences sur liste du stock
  * Params : --
  **********************************************************************/
  var _Tonyryu_SoW_Scene_Equip_createItemWindow = Scene_Equip.prototype.createItemWindow;
  Scene_Equip.prototype.createItemWindow = function() {
    _Tonyryu_SoW_Scene_Equip_createItemWindow.call(this);
    this._itemWindow.setSowEquipWindow(this._sowEquipWindow);
  };
  
  /**********************************************************************
  * Méthode : onSlotCancel (Surcharge)
  * Fonction : Ajoute la mise à jour de la fenêtre listant les compétences
  * Params : --
  **********************************************************************/
  var _Tonyryu_SoW_Scene_Equip_onSlotCancel = Scene_Equip.prototype.onSlotCancel;
  Scene_Equip.prototype.onSlotCancel = function(){
    _Tonyryu_SoW_Scene_Equip_onSlotCancel.call(this);
    this._sowEquipWindow.setItem();
  };

  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification du "classe" Window_SkillList
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : drawItem
  * Fonction : Ajoute le dessin de la jauge dans la fenêtre des compétences
  * Params : --
  **********************************************************************/
  var _Tonyryu_SoW_Window_SkillList_drawItem = Window_SkillList.prototype.drawItem;
  Window_SkillList.prototype.drawItem = function(index) {
    var fontsize = this.contents.fontSize;
    this.contents.fontSize = this.contents.fontSize - 4;
    _Tonyryu_SoW_Window_SkillList_drawItem.call(this, index);
    this.contents.fontSize = fontsize;
    var skill = this._data[index];
    if (skill) {
      if(this._actor.cpSkill(skill.id) < this._actor.cpSkillNeeded(skill.id)){
        var costWidth = this.costWidth();
        var rect = this.itemRect(index);
        rect.width -= (this.textPadding() + costWidth - 5);
        
        this.contents.fillRect(rect.x + rect.width - 80, rect.y + 8, 1, 20, param_cpColorFrame);
        this.contents.fillRect(rect.x + rect.width - 80, rect.y + 28 - 1, 80, 1, param_cpColorFrame);
        this.contents.fillRect(rect.x + rect.width - 80, rect.y + 8, 80, 1, param_cpColorFrame);
        this.contents.fillRect(rect.x + rect.width - 1, rect.y + 8, 1, 20, param_cpColorFrame);
        this.contents.gradientFillRect(rect.x + rect.width - 79, rect.y + 9, 78 * this._actor.skillCpRate(skill.id), 18, param_cpBarColor1, param_cpBarColor2);
        var save_fontsize = this.contents.fontSize ;
        this.contents.fontSize = 16;
        var text = String(this._actor.cpSkill(skill.id)) + "/" + String(this._actor.cpSkillNeeded(skill.id));
        if(this._actor.skillMastered(skill.id))
          text = param_textMastered;
        this.changeTextColor(this.normalColor());
        this.contents.drawText(text, rect.x + rect.width - 75, rect.y + 18, 70, 20);
        this.contents.fontSize = save_fontsize ;
      }
    }
  };

})();
