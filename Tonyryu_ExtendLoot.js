﻿﻿//=============================================================================
// Tonyryu_ExtendLoot.js
//=============================================================================

/*:
 * @plugindesc Permet de modifier le système de loot de base
 * @author Tonyryu
 *

 * @help http://www.tonyryudev.com/
 * 
 */

/*
 * Suivi de version 
 * 1.00 : 15/11/2015 : Tonyryu : Création plugin
 * 
 */

(function() {

  var parameters = PluginManager.parameters('Tonyryu_ExtendLoot');
  
  
  Game_Enemy.prototype.makeDropItems = function() {
    var tabReturn = [];
    var regExp = /\\LOOT\[(.*)\]/;
    var match = regExp.exec(this.enemy().note);
    if(match){
      var listLoot = match[1];
      listLoot = listLoot.replace(' ', '');
      var tabLoot = listLoot.split(';');
      for(var i = 0; i < tabLoot.length; i++){
        var tabDesc = tabLoot[i].split(',');
        var getItem = true;
        if(tabDesc[2]){
          getItem = $gameSwitches.value(tabDesc[2]);
        }
        if(getItem){
          var pourcent = 100;
          var tabDetail = tabDesc[0].split('I');
          var nbr = 0;
          var kind = 0;
          var id = 0;
          if(tabDesc[1])
            pourcent = Math.max(Math.min(100, Number(tabDesc[1])),0);
          
          if( tabDetail.length === 2 ){
            nbr = Number(tabDetail[0] || '1');
            kind = 1;
            id = Number(tabDetail[1]);
          }else{
            tabDetail = tabDesc[0].split('W');
            if( tabDetail.length === 2 ){
              nbr = Number(tabDetail[0] || '1');
              kind = 2;
              id = Number(tabDetail[1]);
            }else{
              tabDetail = tabDesc[0].split('A');
              if( tabDetail.length === 2 ){
                nbr = Number(tabDetail[0] || '1');
                kind = 3;
                id = Number(tabDetail[1]);
              }
            }
          }
          for(var j = 0; j < nbr; j++){
            if((Math.random() * (100/pourcent)) < this.dropItemRate())
              tabReturn = tabReturn.concat(this.itemObject(kind, id));
          }
        }
      }
    }
    return tabReturn;
  };

})();
