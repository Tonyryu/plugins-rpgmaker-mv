//=============================================================================
// Tonyryu_ShopLimitedObject.js
//=============================================================================

/*:
 * @plugindesc Plugin permettant d'ajouter une limite d'objet dans les commerces
 * @author Tonyryu
 *
 * @help http://www.tonyryudev.com/
 * 
 */

/*
 * Suivi de version 
 * 1.00 : 12/04/2017 : Tonyryu : Création plugin
 * 1.01 : 14/04/2017 : Tonyryu : Ajout de la possibilité d'ouvrir un commerce normal
 * 1.02 : 14/04/2017 : Tonyryu : Correction d'un bug qui empéchait de positionner 0 en prix
 * 
 */

(function() {

  var parameters = PluginManager.parameters('Tonyryu_ShopLimitedObject');


  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Party
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : initAllItems (surcharge)
  * Fonction : Ajoute la liste des commerces ouverts
  * Params : --
  **********************************************************************/
  var _Tonyryu_SLO_Game_Party_initAllItems = Game_Party.prototype.initAllItems;
  Game_Party.prototype.initAllItems = function() {
    _Tonyryu_SLO_Game_Party_initAllItems.call(this);
    this._allShopOpen = {};
  };
  
  /**********************************************************************
  * Méthode : tonyryu_SLO_setShop (nouvelle)
  * Fonction : Ajoute un commerce à la liste des commerces ouverts
  * Params : pIdMapEvent : Concaténation Id Map - Id Event
  *          pTabObjet : tableau d'objet restant dans le commerce
  **********************************************************************/
  Game_Party.prototype.tonyryu_SLO_setShop = function(pIdMapEvent, pTabNumber) {
    this._allShopOpen[pIdMapEvent] = pTabNumber;
  };
  
  /**********************************************************************
  * Méthode : tonyryu_SLO_getShop (nouvelle)
  * Fonction : Récupérer le contenu d'un commerce ouvert
  * Params : pIdMapEvent : Concaténation Id Map - Id Event
  **********************************************************************/
  Game_Party.prototype.tonyryu_SLO_getShop = function(pIdMapEvent) {
    return this._allShopOpen[pIdMapEvent];
  };

  /**********************************************************************
  * Méthode : tonyryu_SLO_shopIsOpen (nouvelle)
  * Fonction : Retourne si le coffre a déjà été ouvert
  * Params : pIdMapEvent : Concaténation Id Map - Id Event
  **********************************************************************/
  Game_Party.prototype.tonyryu_SLO_shopIsOpen = function(pIdMapEvent) {
    return !!this._allShopOpen[pIdMapEvent];
  };
  
  /**********************************************************************
  * Méthode : tonyryu_SLO_deleteShop (nouvelle)
  * Fonction : supprime le commerce enregistré, réaprovisionnement
  * Params : pIdMapEvent : Concaténation Id Map - Id Event
  **********************************************************************/
  Game_Party.prototype.tonyryu_SLO_deleteShop = function(pIdMapEvent) {
    if(this._allShopOpen[pIdMapEvent])
      delete this._allShopOpen[pIdMapEvent];
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Window_ShopBuy
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : initialize (surcharge)
  * Fonction : ajout des propriétés permettant d'utiliser le module SLO
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Window_ShopBuy_initialize = Window_ShopBuy.prototype.initialize;
  Window_ShopBuy.prototype.initialize = function(x, y, height, shopGoods){
    tonyryu_SLO_Window_ShopBuy_initialize.call(this, x, y, height, shopGoods);
    this._tabNumber = [];
  };
 
  /**********************************************************************
  * Méthode : tonyryu_SLO_setTabNumber (nouvelle)
  * Fonction : ajout des propriétés permettant d'utiliser le module SLO
  * Params : --
  **********************************************************************/
  Window_ShopBuy.prototype.tonyryu_SLO_setTabNumber = function(pTabNumber) {
    this._tabNumber = pTabNumber;
    this.refresh();
  };
  
  /**********************************************************************
  * Méthode : drawItemName (surcharge)
  * Fonction : décalage du nom de l'objet
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Window_ShopBuy_drawItemName = Window_ShopBuy.prototype.drawItemName;
  Window_ShopBuy.prototype.drawItemName = function(item, x, y, width) {
    tonyryu_SLO_Window_ShopBuy_drawItemName.call(this, item, x + 50, y, width - 50);
  };
  
  /**********************************************************************
  * Méthode : drawItemName (surcharge)
  * Fonction : décalage du nom de l'objet
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Window_ShopBuy_isEnabled = Window_ShopBuy.prototype.isEnabled;
  Window_ShopBuy.prototype.isEnabled = function(item) {
    var enabled = tonyryu_SLO_Window_ShopBuy_isEnabled.call(this, item);
    if(this._tabNumber)
      return enabled && this._tabNumber[this._data.indexOf(item)] > 0;
    else
      return enabled;
  };  
  
  /**********************************************************************
  * Méthode : drawItemName (surcharge)
  * Fonction : ajout de l'affichage du nombre d'objet
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Window_ShopBuy_drawItem = Window_ShopBuy.prototype.drawItem;
  Window_ShopBuy.prototype.drawItem = function(index) {
    tonyryu_SLO_Window_ShopBuy_drawItem.call(this, index);
    var rect = this.itemRect(index);
    var item = this._data[index];
    if(this._tabNumber){
      this.changePaintOpacity(this.isEnabled(item));
      this.drawText(String( this._tabNumber[index]), rect.x, rect.y, 40);
      this.changePaintOpacity(true);
    }
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Scene_Shop
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : initialize (surcharge)
  * Fonction : ajout des propriétés permettant d'utiliser le module SLO
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Scene_Shop_initialize = Scene_Shop.prototype.initialize;
  Scene_Shop.prototype.initialize = function(pTabNumber, pKey){
    tonyryu_SLO_Scene_Shop_initialize.call(this);
    this._tabNumber = [];
    this._keySLO = "";
  };
 
  /**********************************************************************
  * Méthode : tonyryu_SLO_addItemNumber (nouvelle)
  * Fonction : permet la transmission des données SLO
  * Params : --
  **********************************************************************/
  Scene_Shop.prototype.tonyryu_SLO_addItemNumber = function(pKey, pTabNumber){
    this._tabNumber = pTabNumber;
    this._keySLO = pKey;
  };
 
  /**********************************************************************
  * Méthode : createBuyWindow (surcharge)
  * Fonction : permet la transmission des données SLO vers Window_Buy
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Scene_Shop_createBuyWindow = Scene_Shop.prototype.createBuyWindow;
  Scene_Shop.prototype.createBuyWindow = function() {
    tonyryu_SLO_Scene_Shop_createBuyWindow.call(this);
    this._buyWindow.tonyryu_SLO_setTabNumber(this._tabNumber);
  };
  
  /**********************************************************************
  * Méthode : maxBuy (surcharge)
  * Fonction : permet de limiter l'achat au nombre d'objet du marchant
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Scene_Shop_maxBuy = Scene_Shop.prototype.maxBuy;
  Scene_Shop.prototype.maxBuy = function() {
    var max = tonyryu_SLO_Scene_Shop_maxBuy.call(this);
    var index = this._buyWindow.index();
    return Math.min(max, this._tabNumber[index]);
  };
  
  /**********************************************************************
  * Méthode : doBuy (surcharge)
  * Fonction : permet de mettre à jour le tableau de nombre d'objet
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Scene_Shop_doBuy = Scene_Shop.prototype.doBuy;
  Scene_Shop.prototype.doBuy = function(number) {
    tonyryu_SLO_Scene_Shop_doBuy.call(this, number);
    var index = this._buyWindow.index();
    this._tabNumber[index] = this._tabNumber[index] - number;
  };
  
  /**********************************************************************
  * Méthode : popScene (surcharge)
  * Fonction : permet de sauvegarder le tableau de quantité acheté
  * Params : --
  **********************************************************************/
  var tonyryu_SLO_Scene_Shop_popScene = Scene_Shop.prototype.popScene;
  Scene_Shop.prototype.popScene = function() {
    $gameParty.tonyryu_SLO_setShop(this._keySLO, this._tabNumber);
    tonyryu_SLO_Scene_Shop_popScene.call(this);
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Interpreter
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : pluginCommand (surcharge)
  * Fonction : Ajoute la commande TonyryuAlchimie
  * Params : --
  **********************************************************************/
  var _Tonyryu_SLO_Game_Interpreter_executeCommand = Game_Interpreter.prototype.executeCommand;
  Game_Interpreter.prototype.executeCommand = function() {
    var trtShop = false;
    var openShop = false;
    var tabGoods = [];
    var tabNumber = [];

    var command = this.currentCommand();
    if(command){
      if(command.code === 302){
        var params = command.parameters;
        trtShop = true;
        var purchaseOnly = params[4];
        if(params[1] > 0){
          tabGoods.push(params);
          tabNumber.push(99);
        }
        
      }
    }
    
    // si la commande est différente de l'ouverture d'un commerce
    if(!trtShop)
      return _Tonyryu_SLO_Game_Interpreter_executeCommand.call(this);
    else{
      var shopKey = String(this._mapId) + '-' + String(this._eventId);


      // boucler sur les commande suivante
      this._index ++;
      var command = this.currentCommand();
      while(command) {
        var params = command.parameters;
        var objet = [];
        var number = 0;
        if(command.code === 126){
          objet = [0, params[0], 0, 0];
          number = this.operateValue(params[1], params[2], params[3]);
        }else if(command.code === 127){
          objet = [1, params[0], 0, 0];
          number = this.operateValue(params[1], params[2], params[3]);
        }else if(command.code === 128){
          objet = [2, params[0], 0, 0];
          number = this.operateValue(params[1], params[2], params[3]);
        }else if(command.code === 605){
          objet = this.currentCommand().parameters;
          number = 99;
        }else if(command.code === 108){
          var tabParam = command.parameters[0].split(" ");
          if(tabParam[0] === 'SLO_PRICE'){
            if(tabParam[1]){
              tabGoods[tabGoods.length - 1][2] = 1; 
              tabGoods[tabGoods.length - 1][3] = Number(tabParam[1]);
            }
          }
        }else{
          openShop = true;
        }
        
        if(openShop){
          if($gameParty.tonyryu_SLO_shopIsOpen(shopKey)){
            tabNumber = $gameParty.tonyryu_SLO_getShop(shopKey);
          }

          SceneManager.push(Scene_Shop);
          SceneManager.prepareNextScene(tabGoods, purchaseOnly);
          SceneManager._nextScene.tonyryu_SLO_addItemNumber(shopKey, tabNumber);

        }
        if(objet.length > 0){
          tabGoods.push(objet);
          tabNumber.push(number);
        }
        this._index ++;
        var command = this.currentCommand();
      }

      this.terminate();
      return true;
    }
  };
  
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Interpreter
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : pluginCommand (surcharge)
  * Fonction : Ajoute la commande TonyryuAlchimie
  * Params : --
  **********************************************************************/
  var _Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
  Game_Interpreter.prototype.pluginCommand = function(command, args) {
    _Game_Interpreter_pluginCommand.call(this, command, args);
    if(command === 'SLO_INIT'){
      $gameParty.tonyryu_SLO_deleteShop(args[0]);
    }
  };
  
})();
