﻿﻿//=============================================================================
// Tonyryu_MenuCommonEvent.js
//=============================================================================

/*:
 * @plugindesc Plugin d'ajouter une option dans le menu lançant un event commun
 * @author Tonyryu
 *
 * @param NomOption
 * @desc Nom de l'option dans le menu
 * @default Lancer Event
 *
 * @param NumEvent
 * @desc Numero de l'event
 * @default 1
 * 
 * @help http://www.tonyryudev.com/
 * 
 */

(function() {
  var parameters = PluginManager.parameters('Tonyryu_MenuCommonEvent');
  var param_nomOption = String(parameters['NomOption'] || 'Lancer Event');
  var param_numEvent = Number(parameters['NumEvent'] || 1);


  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Window_MenuCommand
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : addMainCommands (surcharge)
  * Fonction : Ajoute l'option de lancement d'événement dans le menu
  * Params : --
  **********************************************************************/
  var _Window_MenuCommand_addMainCommands = Window_MenuCommand.prototype.addMainCommands;
  Window_MenuCommand.prototype.addMainCommands = function() {
    _Window_MenuCommand_addMainCommands.call(this);
      var enabled = this.areMainCommandsEnabled();
      if (this.needsCommand('commonEvent')) {
        this.addCommand(param_nomOption, 'commonEvent', enabled);
    }
  };

  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Scene_Menu
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : createCommandWindow (surcharge)
  * Fonction : Ajoute l'handle sur la commande événement commun
  * Params : --
  **********************************************************************/
  var _Scene_Menu_createCommandWindow = Scene_Menu.prototype.createCommandWindow;
  Scene_Menu.prototype.createCommandWindow = function(){
    _Scene_Menu_createCommandWindow.call(this);
    this._commandWindow.setHandler('commonEvent', this.commandCommonEvent.bind(this));
  };

  /**********************************************************************
  * Méthode : commandCommonEvent (surcharge)
  * Fonction : Permet de lancer l'événement commun
  * Params : --
  **********************************************************************/
  Scene_Menu.prototype.commandCommonEvent = function() {
    // Retour sur scene_map
    this.popScene();
    //SceneManager._nextScene._launchCommonEvent = true;
    $gameTemp.reserveCommonEvent(param_numEvent);

  };
})();
