//=============================================================================
// Tonyryu_Chest.js
//=============================================================================

/*:
 * @plugindesc Plugin permettant d'afficher une fenêtre de contenu de coffre via un événement
 * @author Tonyryu
 *
 * @param Window X
 * @desc Coordonnée X de la fenêtre
 * @default 50
 *
 * @param Window Y
 * @desc Coordonnée Y de la fenêtre
 * @default 50
 *
 * @param Window Width
 * @desc Largeur de la fenêtre
 * @default 400
 *
 * @param Window Items Number
 * @desc Nombre d'objet visible dans la fenêtre
 * @default 5
 * 
 * @param Open sound
 * @desc Nom du SE à jouer à l'ouverture du coffre
 * @default Chest1
 *
 * @param Close sound
 * @desc Nom du SE à jouer à la fermeture du coffre
 * @default Close1
 *
 * @help http://www.tonyryudev.com/
 * 
 */

/*
 * Suivi de version 
 * 1.00 : 15/03/2016 : Tonyryu : Création plugin
 * 1.01 : 17/03/2016 : Tonyryu : Correction plantage si coffre vide
 * 
 */

(function() {

  var parameters = PluginManager.parameters('Tonyryu_Chest');
  var _Tonyryu_Chest_windowX = Number(parameters['Window X'] || 50);
  var _Tonyryu_Chest_windowY = Number(parameters['Window Y'] || 50);
  var _Tonyryu_Chest_windowWidth = Number(parameters['Window Width'] || 300);
  var _Tonyryu_Chest_windowItemsNumber = Number(parameters['Window Items Number'] || 5);
  var _Tonyryu_Chest_openSound = String(parameters['Open sound'] || 'Chest1');
  var _Tonyryu_Chest_closeSound = String(parameters['Close sound'] || 'Close1');

  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Temp
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : initialize (surcharge)
  * Fonction : Ajoute de propriété pour gestion du coffre
  * Params : --
  **********************************************************************/
  var _Tonyryu_Chest_Game_Temp_initialize = Game_Temp.prototype.initialize;
  Game_Temp.prototype.initialize = function(){
    _Tonyryu_Chest_Game_Temp_initialize.call(this);
    this._tabObjetsCoffre = [];
    this._coffreIdMap = 0;
    this._coffreIdEvent = 0;
    this._coffreActif = false;
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Player
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : canMove (surcharge)
  * Fonction : Ajoute la vérification de l'état du coffre pour interdir déplacement
  * Params : --
  **********************************************************************/
  var _Tonyryu_Chest_Game_Player_canMove = Game_Player.prototype.canMove;
  Game_Player.prototype.canMove = function() {
    if($gameTemp._coffreActif) {
      return false;
    }
    return _Tonyryu_Chest_Game_Player_canMove.call(this);
  };

  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Party
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : initAllItems (surcharge)
  * Fonction : Ajoute la liste des coffres ouverts
  * Params : --
  **********************************************************************/
  var _Tonyryu_Chest_Game_Party_initAllItems = Game_Party.prototype.initAllItems;
  Game_Party.prototype.initAllItems = function() {
    _Tonyryu_Chest_Game_Party_initAllItems.call(this);
    this._allChestOpen = {};
  };
  
  /**********************************************************************
  * Méthode : tonyryu_chest_setChest (surcharge)
  * Fonction : Ajoute un coffre à la liste des coffres ouverts
  * Params : pIdMapEvent : Concaténation Id Map - Id Event
  *          pTabObjet : tableau d'objet restant dans le coffre
  **********************************************************************/
  Game_Party.prototype.tonyryu_chest_setChest = function(pIdMapEvent, pTabObjet) {
    this._allChestOpen[pIdMapEvent] = pTabObjet.slice(0);
  };
  
  /**********************************************************************
  * Méthode : tonyryu_chest_getChest (surcharge)
  * Fonction : Récupérer le contenu d'un coffre ouvert
  * Params : pIdMapEvent : Concaténation Id Map - Id Event
  **********************************************************************/
  Game_Party.prototype.tonyryu_chest_getChest = function(pIdMapEvent) {
    return this._allChestOpen[pIdMapEvent];
  };

  /**********************************************************************
  * Méthode : tonyryu_chest_chestIsOpen (surcharge)
  * Fonction : Retourne si le coffre a déjà été ouvert
  * Params : pIdMapEvent : Concaténation Id Map - Id Event
  **********************************************************************/
  Game_Party.prototype.tonyryu_chest_chestIsOpen = function(pIdMapEvent) {
    return !!this._allChestOpen[pIdMapEvent];
  };

  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Création d'une nouvelle "classe" Window_Tonyryu_Chest
  *  hérite de la classe Window_Selectable
  *----------------------------------------------------------------------
  **********************************************************************/
  function Window_Tonyryu_Chest() {
    this.initialize.apply(this, arguments);
  }

  Window_Tonyryu_Chest.prototype = Object.create(Window_Selectable.prototype);
  Window_Tonyryu_Chest.prototype.constructor = Window_Tonyryu_Chest;
  
  /**********************************************************************
  * Méthode : initialize
  * Fonction : Initialisation de la "classe" Window_Tonyryu_Chest
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.initialize = function() {
    Window_Selectable.prototype.initialize.call(this, _Tonyryu_Chest_windowX, _Tonyryu_Chest_windowY, _Tonyryu_Chest_windowWidth, (_Tonyryu_Chest_windowItemsNumber * this.itemHeight()) + (18 * 2));
    this.openness = 0;
    this.active = true;
    this.setHandler('ok', this.takeItem.bind(this));
    this.setHandler('cancel', this.closeChest.bind(this));
  };
 
  /**********************************************************************
  * Méthode : maxCols (surcharge)
  * Fonction : Retourne le nombre maximum de colonne
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.maxCols = function() {
    return 1;
  };

  /**********************************************************************
  * Méthode : maxItems (surcharge)
  * Fonction : Retourne le nombre d'item maximum
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.maxItems = function() {
    return $gameTemp._tabObjetsCoffre.length;
  };

  /**********************************************************************
  * Méthode : refresh
  * Fonction : recré le contenu affichable
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.refresh = function() {
    this.createContents();
    this.drawAllItems();
  };
  
  /**********************************************************************
  * Méthode : isCurrentItemEnabled (replace)
  * Fonction : permet de buzzer si pas d'item sélectionné
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.isCurrentItemEnabled = function() {
    return (this._index > -1);
  };
  
  /**********************************************************************
  * Méthode : drawItem
  * Fonction : dessine le nom d'un objet dans la liste
  * Params : index : index de l'objet dans le tableau
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.drawItem = function(index){
    var item = $gameTemp._tabObjetsCoffre[index];
    var rect = this.itemRect(index);
    this.resetTextColor();
    if(item.type === 'gold'){
      var valueWidth = Math.min(rect.width - 84, this.textWidth(item.number));
      this.drawText(item.number, rect.x, rect.y, valueWidth);
      this.changeTextColor(this.systemColor());
      this.drawText(TextManager.currencyUnit, rect.x + valueWidth + 4 , rect.y, rect.width - valueWidth - 4);
    }else if(item.type === 'item'){
      this.drawText(item.number, rect.x, rect.y, 40);
      this.drawItemName($dataItems[item.id], rect.x + 46, rect.y, rect.width - 50);
    }else if(item.type === 'weapon'){
      this.drawText(item.number, rect.x, rect.y, 40);
      this.drawItemName($dataWeapons[item.id], rect.x + 46, rect.y, rect.width - 50);
    }else if(item.type === 'armor'){
      this.drawText(item.number, rect.x, rect.y, 40);
      this.drawItemName($dataArmors[item.id], rect.x + 46, rect.y, rect.width - 50);
    }
  };
  
  /**********************************************************************
  * Méthode : open (surcharge)
  * Fonction : permet d'ajouter l'activation du premier élément
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.open = function() {
    this.refresh();
    this.deselect();
    if($gameTemp._tabObjetsCoffre.length > 0){
      this.select(0);
    }
    Window_Selectable.prototype.open.call(this);
  };
  
  /**********************************************************************
  * Méthode : update (surcharge)
  * Fonction : ajoute le controle de l'ouverture du coffre via Game_Temp
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.update = function() {
    Window_Selectable.prototype.update.call(this);
    if(!this.isOpening() && !this.isClosing()) {
      if(!this.isOpen() && $gameTemp._coffreActif){
        this._scrollY = 0;
        $gameMap._events[$gameTemp._coffreIdEvent]._direction = 8;
        this.open();
        return;
      }
    }
  };
  
  /**********************************************************************
  * Méthode : takeItem
  * Fonction : permet de gérer le choix d'un objet dans le coffre
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.takeItem = function() {
    // Ajouter l'objet dans l'inventaire
    var item = $gameTemp._tabObjetsCoffre[this._index];
    if(item){
      this.activate();
      if(item.type === 'gold'){
        $gameParty.gainGold(item.number);
      }else if(item.type === 'item'){
        $gameParty.gainItem($dataItems[item.id], item.number);
      }else if(item.type === 'weapon'){
        $gameParty.gainItem($dataWeapons[item.id], item.number);
      }else if(item.type === 'armor'){
        $gameParty.gainItem($dataArmors[item.id], item.number);
      }

      // retirer l'objet du coffre
      $gameTemp._tabObjetsCoffre.splice(this._index, 1);

      // replacer curseur
      if($gameTemp._tabObjetsCoffre.length === this._index)
        this.select(this._index - 1);

      // rafraichir le contenu du coffre
      this.refresh();
    }
  };
  
  /**********************************************************************
  * Méthode : closeChest
  * Fonction : permet de fermer la fenêtre
  * Params : --
  **********************************************************************/
  Window_Tonyryu_Chest.prototype.closeChest = function() {
    this.activate();
    AudioManager.playSe({name:_Tonyryu_Chest_closeSound,pan:0, pitch:100, volume:90});
    this.close();
    $gameMap._events[$gameTemp._coffreIdEvent]._direction = 2;
    $gameParty.tonyryu_chest_setChest(String($gameTemp._coffreIdMap) + '-' + String($gameTemp._coffreIdEvent), $gameTemp._tabObjetsCoffre);
    $gameTemp._coffreActif = false;
  };
 
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Scene_Map
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : createAllWindows (surcharge)
  * Fonction : Ajoute la fenêtre de coffre sur scene_map
  * Params : --
  **********************************************************************/
  var _Tonyryu_Chest_Scene_Map_createAllWindows = Scene_Map.prototype.createAllWindows;
  Scene_Map.prototype.createAllWindows = function() {
    _Tonyryu_Chest_Scene_Map_createAllWindows.call(this);
    this._Tonyryu_Chest_Window = new Window_Tonyryu_Chest();
    this.addWindow(this._Tonyryu_Chest_Window);
  };

  /**********************************************************************
  * Méthode : isMenuEnabled (surcharge)
  * Fonction : interdit l'ouverture du menu si coffre ouvert
  * Params : --
  **********************************************************************/
  var _Tonyryu_Chest_Scene_Map_isMenuEnabled = Scene_Map.prototype.isMenuEnabled;
  Scene_Map.prototype.isMenuEnabled = function() {
    return _Tonyryu_Chest_Scene_Map_isMenuEnabled.call(this) && !$gameTemp._coffreActif;
  };
  
  /**********************************************************************
  *----------------------------------------------------------------------
  * Modification de la "classe" Game_Interpreter
  *----------------------------------------------------------------------
  **********************************************************************/
  /**********************************************************************
  * Méthode : pluginCommand (surcharge)
  * Fonction : Ajoute la commande TonyryuAlchimie
  * Params : --
  **********************************************************************/
  var _Tonyryu_Chest_Game_Interpreter_executeCommand = Game_Interpreter.prototype.executeCommand;
  Game_Interpreter.prototype.executeCommand = function() {
    var trtCoffre = false;
    if(this._index === 0){
      var command = this.currentCommand();
      if(command){
        if(command.code === 108){
          if(command.parameters[0] === 'COFFRE'){
            trtCoffre = true;
          }
        }
      }
    }
    // si la première commande n'est pas le commentaire 'COFFRE'
    if(!trtCoffre)
      return _Tonyryu_Chest_Game_Interpreter_executeCommand.call(this);
    else{
      AudioManager.playSe({name:_Tonyryu_Chest_openSound,pan:0, pitch:100, volume:90});
      // TODO changer l'aspect du coffre
      $gameTemp._coffreIdMap = this._mapId;
      $gameTemp._coffreIdEvent = this._eventId;
      
      $gameTemp._coffreActif = true;
      if(!$gameParty.tonyryu_chest_chestIsOpen(String($gameTemp._coffreIdMap) + '-' + String($gameTemp._coffreIdEvent))){
        $gameTemp._tabObjetsCoffre.length = 0;

        // boucler sur les commande suivante
        this._index ++;
        var command = this.currentCommand();
        while(command) {
          var params = command.parameters;
          var objet = {};
          if(command.code === 125){
            objet = {type: 'gold', id: 0, number: this.operateValue(params[0], params[1], params[2])};
          }else if(command.code === 126){
            objet = {type: 'item',id: params[0],number: this.operateValue(params[1], params[2], params[3])};
          }else if(command.code === 127){
            objet = {type: 'weapon',id: params[0],number: this.operateValue(params[1], params[2], params[3])};
          }else if(command.code === 128){
            objet = {type: 'armor',id: params[0],number: this.operateValue(params[1], params[2], params[3])};
          }
          if(objet.type){
            $gameTemp._tabObjetsCoffre.push(objet);
          }
          this._index ++;
          var command = this.currentCommand();
        }
        
      }else{
        $gameTemp._tabObjetsCoffre = $gameParty.tonyryu_chest_getChest(String($gameTemp._coffreIdMap) + '-' + String($gameTemp._coffreIdEvent));
      }
      
      
      this.terminate();
      return true;
    }
  };

  
})();
