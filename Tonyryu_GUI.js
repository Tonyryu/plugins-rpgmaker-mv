﻿//=============================================================================
// Tonyryu_GUI.js
//=============================================================================

/*:
 * @plugindesc Plugin permettant l'ajout de GUI (0.8.2)
 * @author Tonyryu
 *
 *
 * @help http://www.tonyryudev.com/
 * 
 */

/**********************************************************************
*----------------------------------------------------------------------
* Modification de la "classe" Bitmap
*----------------------------------------------------------------------
**********************************************************************/
Input.keyMapper[8] = 'backspace';
Input.keyMapper[13] = 'enter';
Input.keyMapper[32] = 'space';
Input.keyMapper[35] = 'end';
Input.keyMapper[36] = 'home';
Input.keyMapper[46] = 'delete';
Input.keyMapper[81] = 'Q';
Input.keyMapper[87] = 'W';
Input.keyMapper[88] = 'X';
Input.keyMapper[90] = 'Z';


/**********************************************************************
* Méthode : clear (surcharge)
* Fonction : Ajoute la propriété this._letter
* Params : --
**********************************************************************/
var _TonyryuGui_Input_clear = Input.clear;
Input.clear = function() {
  _TonyryuGui_Input_clear.call(this);
  this._letter = '';
  this._countLetterFrame = 0;
};

/**********************************************************************
* Méthode : isPressed (surcharge)
* Fonction : vérifier space ou enter si ok
* Params : --
**********************************************************************/
var _TonyryuGui_Input_isPressed = Input.isPressed;
Input.isPressed = function(keyName) {
  if(keyName === 'ok')
    return _TonyryuGui_Input_isPressed.call(this, 'space') || _TonyryuGui_Input_isPressed.call(this, 'enter') || _TonyryuGui_Input_isPressed.call(this, 'ok');
  else
    return _TonyryuGui_Input_isPressed.call(this, keyName);
};

/**********************************************************************
* Méthode : isTriggered (surcharge)
* Fonction : vérifier space ou enter si ok
* Params : --
**********************************************************************/
var _TonyryuGui_Input_isTriggered = Input.isTriggered;
Input.isTriggered = function(keyName) {
  if(keyName === 'ok')
    return _TonyryuGui_Input_isTriggered.call(this, 'space') || _TonyryuGui_Input_isTriggered.call(this, 'enter') || _TonyryuGui_Input_isTriggered.call(this, 'ok');
  else
    return _TonyryuGui_Input_isTriggered.call(this, keyName);
};

/**********************************************************************
* Méthode : isRepeated (surcharge)
* Fonction : vérifier space ou enter si ok
* Params : --
**********************************************************************/
var _TonyryuGui_Input_isRepeated = Input.isRepeated;
Input.isRepeated = function(keyName) {
  if(keyName === 'ok')
    return _TonyryuGui_Input_isRepeated.call(this, 'space') || _TonyryuGui_Input_isRepeated.call(this, 'enter') || _TonyryuGui_Input_isRepeated.call(this, 'ok');
  else
    return _TonyryuGui_Input_isRepeated.call(this, keyName);
};

/**********************************************************************
* Méthode : isLongPressed (surcharge)
* Fonction : vérifier space ou enter si ok
* Params : --
**********************************************************************/
var _TonyryuGui_Input_isLongPressed = Input.isLongPressed;
Input.isLongPressed = function(keyName) {
  if(keyName === 'ok')
    return _TonyryuGui_Input_isLongPressed.call(this, 'space') || _TonyryuGui_Input_isLongPressed.call(this, 'enter') || _TonyryuGui_Input_isLongPressed.call(this, 'ok');
  else
    return _TonyryuGui_Input_isLongPressed.call(this, keyName);
};


/**********************************************************************
* Méthode : _setupEventHandlers (surcharge)
* Fonction : Ajoute le listener d'événement keypress
* Params : --
**********************************************************************/
var _TonyryuGui_Input_setupEventHandlers = Input._setupEventHandlers;
Input._setupEventHandlers = function() {
  _TonyryuGui_Input_setupEventHandlers.call(this);
  document.addEventListener('keypress', this._onKeyPress.bind(this));
};

/**********************************************************************
* Méthode : update (surcharge)
* Fonction : Ajoute la suppression de la lettre au second cycle
* Params : --
**********************************************************************/
var _TonyryuGui_Input_update = Input.update;
Input.update = function() {
  _TonyryuGui_Input_update.call(this);
  if(this._letter !== ''){
    if(this._countLetterFrame === 2)
      this._letter = '';
    else
      this._countLetterFrame ++;
  }
};

/**********************************************************************
* Méthode : _onKeyPress (remplacement)
* Fonction : permet de gérer l'événement keypress et de stocker la lettre
* Params : event : evénément déclenchant l'appel de la fonction
**********************************************************************/
Input._onKeyPress = function(event) {
  event = event || window.event;
  var charCode = event.which || event.keyCode;
  if(charCode !== 13){
    var charStr = String.fromCharCode(charCode);
    this._letter = charStr;
    this._countLetterFrame = 1;
  }
};

/**********************************************************************
* Méthode : getLetter
* Fonction : permet de récupérer la lettre enregistré
* Params : --
**********************************************************************/
Input.getLetter = function(){
  return this._letter;
};

/**********************************************************************
*----------------------------------------------------------------------
* Modification de la "classe" Bitmap
*----------------------------------------------------------------------
**********************************************************************/
/**
 * Fills the specified rectangle.
 *
 * @method drawFrame
 * @param {Number} x The x coordinate for the upper-left corner
 * @param {Number} y The y coordinate for the upper-left corner
 * @param {Number} width The width of the rectangle to clear
 * @param {Number} height The height of the rectangle to clear
 * @param {String} color The color of the rectangle in CSS format
 * @param {Number} size The size of line
 */
Bitmap.prototype.drawFrame = function(x, y, width, height, color, size) {
    size = (size || 1);
    this.fillRect(x, y, size, height, color);
    this.fillRect(x, y + height - size, width, size, color);
    this.fillRect(x, y, width, size, color);
    this.fillRect(x + width - size, y, size, height, color);
};


/**
 * Fills the specified rectangle.
 *
 * @method drawFrame
 * @param {Number} x1 The x coordinate for the line start
 * @param {Number} y2 The y coordinate for the line start
 * @param {Number} x2 The x coordinate for the line end
 * @param {Number} y2 The y coordinate for the line end
 * @param {String} color The color of the rectangle in CSS format
 * @param {Number} width Width of line
 */
Bitmap.prototype.drawLine = function(x1, y1, x2, y2, color, width) {
  var context = this._context;
  context.save();
  width = (width || 1);
  context.beginPath();
  context.moveTo(x1, y1);
  context.lineTo(x2, y2);
  context.lineWidth = width;

  // set line color
  context.strokeStyle = color;
  context.stroke();
  
  context.restore();
  this._setDirty();
};


/**********************************************************************
*----------------------------------------------------------------------
* Modification de la "classe" TouchInput
*----------------------------------------------------------------------
**********************************************************************/
/**********************************************************************
* Méthode : _onMouseMove (remplacement)
* Fonction : Mettre à jour les coordonnées de TouchInput x et y à chaque cycle
* Params : event : Evénement lié à la souris sur le canvas
**********************************************************************/
TouchInput._onMouseMove = function(event) {
  var x = Graphics.pageToCanvasX(event.pageX);
  var y = Graphics.pageToCanvasY(event.pageY);
  this._onMove(x, y);
};

/**********************************************************************
*----------------------------------------------------------------------
* Modification du "module" SceneManager
*----------------------------------------------------------------------
**********************************************************************/
/**********************************************************************
* Méthode : onSceneCreate (surcharge)
* Fonction : Ajoute la création du sprite drag&drop
* Params : window : Fenêtre à ajouter
**********************************************************************/
var _TonyryuGui_SceneManager_onSceneCreate = SceneManager.onSceneCreate;
SceneManager.onSceneCreate = function() {
  _TonyryuGui_SceneManager_onSceneCreate.call(this);
  this._scene._dragDropSprite = new Sprite();
  this._scene._dragDropSprite.visible = false;
  this._scene._dragDropSprite.opacity = 120;
  this._scene.addChild(this._scene._dragDropSprite);
  this._scene._popupSprite = new Sprite();
  this._scene._popupSprite.bitmap = new Bitmap();
  this._scene._popupSprite.bitmap.textColor = '#000000';
  this._scene._popupSprite.bitmap.fontFace = 'GameFont';
  this._scene._popupSprite.bitmap.fontSize = 12;
  this._scene._popupSprite.bitmap.fontItalic = false;
  this._scene._popupSprite.bitmap.outlineWidth = 0;
  this._scene._popupFrameColor = '#000000';
  this._scene._popupBkgColor = '#ffff99';
  this._scene._popupSprite.visible = false;
  this._scene.addChild(this._scene._popupSprite);
};

SceneManager._dragDropGuiSrc = null;
SceneManager._dragDropGuiDest = null;
SceneManager._dragDropData = null;
SceneManager._dragInProgress = false;
//SceneManager._popupInProgress = false;

/**********************************************************************
* Méthode : update (surcharge)
* Fonction : Ajoute le déplacement du sprite pour suivre la souris
* Params : --
**********************************************************************/
var _TonyryuGui_SceneManager_update = SceneManager.update;
SceneManager.update = function() {
  _TonyryuGui_SceneManager_update.call(this);
  if(TouchInput.isReleased())
    SceneManager.initDragDrop();
  if(SceneManager._dragInProgress){
    if(this._scene._dragDropSprite.bitmap){
      this._scene._dragDropSprite.x = TouchInput._x - (Math.floor(this._scene._dragDropSprite.bitmap.width / 2));
      this._scene._dragDropSprite.y = TouchInput._y;
      this._scene._dragDropSprite.visible = true;
    }
  }
};

/**********************************************************************
* Méthode : initDragDrop (surcharge)
* Fonction : Permet de réinitialiser les données de drag and drop
* Params : --
**********************************************************************/
SceneManager.initDragDrop = function() {
  this._scene._dragDropSprite.visible = false;
  delete this._scene._dragDropSprite.bitmap;
  SceneManager._dragDropGuiSrc = null;
  SceneManager._dragDropGuiDest = null;
  SceneManager._dragDropData = null;
  SceneManager._dragInProgress = false;
};

/**********************************************************************
* Méthode : startDragDrop
* Fonction : Permet de démarrer un drag and drop
* Params : --
**********************************************************************/
SceneManager.startDragDrop = function(pGuiSrc, pData) {
  if(!SceneManager._dragInProgress && pData !== null){
    SceneManager._dragDropGuiSrc = pGuiSrc;
    SceneManager._dragDropData = pData;
    this._scene._dragDropSprite.bitmap = SceneManager._dragDropGuiSrc.bitmap;
    SceneManager._dragInProgress = true;
  }
};

/**********************************************************************
* Méthode : validDrop
* Fonction : Permet de valider un drag and drop
* Params : --
**********************************************************************/
SceneManager.validDrop = function(pGuiDest) {
  if(SceneManager._dragInProgress){
    if(pGuiDest._dropCode.indexOf(SceneManager._dragDropGuiSrc._dragCode) !== -1){
      SceneManager._dragDropGuiSrc._dragData = pGuiDest._dragData;
      pGuiDest._dragData = SceneManager._dragDropData;
      SceneManager._dragDropGuiSrc.onDrag();
      pGuiDest.onDrop();
      SceneManager.initDragDrop();
    }
  }
};

/**********************************************************************
* Méthode : showPopup
* Fonction : affiche ou supprime la popup d'information
* Params : pGuiSrc : Gui qui déclenche l'affichage de la popup
*          pShow : booleen 
**********************************************************************/
SceneManager.showPopup = function(pGuiSrc, pShow) {
  if(pShow){
    if(pGuiSrc._popupText !== ''){

      var textWidth = this._scene._popupSprite.bitmap.measureTextWidth(pGuiSrc._popupText) + 10;
      this._scene._popupSprite.bitmap.resize(textWidth , this._scene._popupSprite.bitmap.fontSize + 10);
      this._scene._popupSprite.width = textWidth;
      this._scene._popupSprite.height = this._scene._popupSprite.bitmap.fontSize + 10;

      this._scene._popupSprite.bitmap.fillRect(0, 0, this._scene._popupSprite.bitmap.width, this._scene._popupSprite.bitmap.height, this._scene._popupBkgColor);
      this._scene._popupSprite.bitmap.drawFrame(0, 0, this._scene._popupSprite.bitmap.width, this._scene._popupSprite.bitmap.height, this._scene._popupFrameColor, 1);
      this._scene._popupSprite.bitmap.drawText(pGuiSrc._popupText, 0, 0, this._scene._popupSprite.bitmap.width, this._scene._popupSprite.bitmap.height, 'center');
      this._scene._popupSprite.visible = true;

      this._scene._popupSprite.x = Math.max(0, TouchInput._x - (Math.max(TouchInput._x + textWidth, Graphics.width) - Graphics.width));
      this._scene._popupSprite.y = TouchInput._y + 25 - (Math.max(TouchInput._y + 25 + this._scene._popupSprite.bitmap.fontSize + 10, Graphics.height) - Graphics.height);
      
    }
  }else
    this._scene._popupSprite.visible = false;
  
};

/**********************************************************************
*----------------------------------------------------------------------
* Modification de la "classe" Scene_Base
*----------------------------------------------------------------------
**********************************************************************/
/**********************************************************************
* Méthode : addWindow (surcharge)
* Fonction : Créer le windowLayer si il n'existe pas avant l'ajout des fenêtre
* Params : window : Fenêtre à ajouter
**********************************************************************/
var _TonyryuGui_Scene_Base_addWindow = Scene_Base.prototype.addWindow;
Scene_Base.prototype.addWindow = function(window) {
  if(!this._windowLayer)
    this.createWindowLayer();
  _TonyryuGui_Scene_Base_addWindow.call(this, window);
};


/**********************************************************************
*----------------------------------------------------------------------
* Création d'une nouvelle "classe" Window_Gui
*  hérite de la classe Window_Base
*----------------------------------------------------------------------
**********************************************************************/
function Window_Gui() {
  this.initialize.apply(this, arguments);
}

Window_Gui.prototype = Object.create(Window_Base.prototype);
Window_Gui.prototype.constructor = Window_Gui;

/**********************************************************************
* Méthode : initialize
* Fonction : Initialisation de la "classe" Window_Gui
* Params : x, y, width, height : coordonnées de la fenêtre
**********************************************************************/
Window_Gui.prototype.initialize = function(x, y, width, height) {
  Window_Base.prototype.initialize.call(this, x, y, width, height);
  this._tabGui = [];
  this._guiHasFocus = '';
  this._handlers = {};
  this.deactivate();
};

/**********************************************************************
* Méthode : addGui
* Fonction : Ajoute un Gui sur la fenêtre
* Params : pSymbol : nom du Gui
*          pGui : instance du Gui
**********************************************************************/
Window_Gui.prototype.addGui = function(pSymbol, pGui) {
  this._tabGui[pSymbol] = pGui;
  this._windowContentsSprite.addChild(this._tabGui[pSymbol]);
};

/**********************************************************************
* Méthode : getGui
* Fonction : Permet de récupérer un Gui positionné sur la fenêtre
* Params : pSymbol : nom du Gui
**********************************************************************/
Window_Gui.prototype.getGui = function(pSymbol) {
  return this._tabGui[pSymbol];
};

/**********************************************************************
* Méthode : update (surcharge)
* Fonction : Permet d'appeler la mise à jour de l'état des gui en fonction
*             de la position de la souris
* Params : --
**********************************************************************/
Window_Gui.prototype.update = function(){
  Window_Base.prototype.update.call(this);
  if(this.active){
    var tabPressed = Input.isTriggered('pagedown');
    var firstGui = '';
    var focusNext = false;
    
    var x = this.canvasToLocalX(TouchInput.x) - this.padding;
    var y = this.canvasToLocalY(TouchInput.y) - this.padding;
    var guiFocus = '';
    
    for(var gui in this._tabGui){
      if(this._tabGui[gui] instanceof Gui_Base){
        if(firstGui === '' && this._tabGui[gui]._focusable)
          firstGui = gui;
        this._tabGui[gui].updateMousePos(x,y);
        if(tabPressed && focusNext && this._tabGui[gui]._focusable){
          this._tabGui[gui].setFocus(true);
          focusNext = false;
          tabPressed = false;
        }
        if(this._tabGui[gui]._focus){ 
          if(this._guiHasFocus !== gui)
            guiFocus = gui;
          else if(tabPressed)
            focusNext = true;
        }
      }
    }
    
    if(tabPressed && firstGui !== ''){
      this._tabGui[firstGui].setFocus(true);
      guiFocus = firstGui;
    }
      
    if(guiFocus !== '' && guiFocus !== this._guiHasFocus){
      if(this._guiHasFocus !== '')
        this._tabGui[this._guiHasFocus].setFocus(false);
      this._guiHasFocus = guiFocus;
    }
    
    if(this._guiHasFocus !== '') {
      this.setCursorRect(this._tabGui[this._guiHasFocus].x, this._tabGui[this._guiHasFocus].y, this._tabGui[this._guiHasFocus].width, this._tabGui[this._guiHasFocus].height);
    } else {
      this.setCursorRect(0, 0, 0, 0);
    }
    
  } 
};

/**********************************************************************
* Méthode : setHandlerGui
* Fonction : Permet de positionner un handler sur un event
* Params : --
**********************************************************************/
Window_Gui.prototype.setHandlerGui = function(guiName, symbol, method) {
  if(this._tabGui[guiName])
    this._tabGui[guiName].setHandler(symbol, method);
};


/**********************************************************************
* Méthode : guisSetOptions
* Fonction : Permet de positionner des options sur un tableau de GUI
* Params : --
**********************************************************************/
Window_Gui.prototype.guisSetOptions = function(pTabGuis, pOptions) {
  for(var idx = 0; idx < pTabGuis.length; idx++){
    this._tabGui[pTabGuis[idx]].setOptions(pOptions);
  }
};

/**********************************************************************
* Méthode : guisGroupSetOptions
* Fonction : Permet de positionner des options sur un groupe de GUI
* Params : --
**********************************************************************/
Window_Gui.prototype.guisGroupSetOptions = function(pGroup, pOptions) {
  for(var gui in this._tabGui){
    if(this._tabGui[gui] instanceof Gui_Base){
      if(this._tabGui[gui]._groupe === pGroup)
        this._tabGui[gui].setOptions(pOptions);
    }
  }
};

/**********************************************************************
*----------------------------------------------------------------------
* Création d'une nouvelle "classe" Gui_Base
*  hérite de la classe Sprite
*----------------------------------------------------------------------
**********************************************************************/
function Gui_Base() {
  this.initialize.apply(this, arguments);
}

Gui_Base.prototype = Object.create(Sprite.prototype);
Gui_Base.prototype.constructor = Gui_Base;

/**********************************************************************
* Méthode : initialize (surcharge)
* Fonction : Permet d'initialiser toutes les propriétés du Gui
* Params : pOptions : Json contenant la liste des options à modifier
**********************************************************************/
Gui_Base.prototype.initialize = function(pOptions) {
  Sprite.prototype.initialize.call(this);
  this._groupe = '';
  this._active = true;
  this._focus = false;
  this._focusable = false;
  this._rollover = false;
  this._click = false;
  this._imageState = 0;
  this._imageStateMax = 1;
  this._backColor = '';
  this._frameColor = '';
  this._needCreateBitmap = true;
  this._imageUrl = '';
  this._needWaitLoad = false;
  this._bitmapUrl = null;
  this._rectBitmap = null;
  this._padding = 0;
  this._offsetImgFragment = 0;
  this._text = '';
  this._textAlign = '';
  this._textColor = '#ffffff';
  this._fontFace = 'GameFont';
  this._fontSize = 28;
  this._fontItalic = false;
  this._rollover = false;
  this._validOnSpace = true;
  this._selectColor = '#aaaaaa';
  this._selectFrameColor = '#ffffff';
  this._selectSprite = new Sprite();
  this._selectSprite.opacity = 100;
  this._selectSprite.x = -2;
  this._selectSprite.y = -2;
  this._selectSprite.visible = false;
  this.addChild(this._selectSprite);
  this._dragData = null;
  this._dropCode = [];
  this._dragCode = '';
  this._selectSensOpacity = -3;
  this._cptFrameBeforePopup = 0;
  this._nbrFrameForPopup = 40;
  this._popupText = '';
  this._handlers = {};
  this._data = null;
  if(pOptions)
    this.setOptions(pOptions);
};

/**********************************************************************
* Méthode : setOptions
* Fonction : Permet de modifier les options
* Params : pOptions : Json des options à modifier
**********************************************************************/
Gui_Base.prototype.setOptions = function(pOptions) {
  for(var nomOption in pOptions){
    var value = pOptions[nomOption];
    if(nomOption === 'groupe')
      this._groupe = value;
    if(nomOption === 'active')
      this._active = value;
    if(nomOption === 'x')
      this.x = value;
    if(nomOption === 'y')
      this.y = value;
    if(nomOption === 'width'){
      this.width = value;
      this._needCreateBitmap = true;
    }
    if(nomOption === 'offsetImgFragment'){
      this._offsetImgFragment = value;
      this._needCreateBitmap = true;
    }
    if(nomOption === 'height'){
      this.height = value;
      this._needCreateBitmap = true;
    }
    if(nomOption === 'imageUrl'){
      if(this._imageUrl !== value){
        if(value === '')
          this._bitmapUrl = null;
        else{
          this._bitmapUrl = Bitmap.load(value);
          this._needWaitLoad = true;
        }
      }
      this._imageUrl = value;
      this._needCreateBitmap = true;
    }
    if(nomOption === 'rectBitmap')
      this._rectBitmap = value;
    if(nomOption === 'backColor')
      this._backColor = value;
    if(nomOption === 'frameColor')
      this._frameColor = value;      
    if(nomOption === 'text')
      this._text = value;
    if(nomOption === 'textColor')
      this._textColor = value;
    if(nomOption === 'textAlign')
      this._textAlign = value;
    if(nomOption === 'fontFace')
      this._fontFace = value;
    if(nomOption === 'fontSize')
      this._fontSize = value;
    if(nomOption === 'fontItalic')
      this._fontItalic = value;
    if(nomOption === 'padding')
      this._padding = value;
    if(nomOption === 'visible')
      this.visible = value;
    if(nomOption === 'focusable')
      this._focusable = value;
    if(nomOption === 'validOnSpace')
      this._validOnSpace = value;
    if(nomOption === 'selectColor')
      this._selectColor = value;
    if(nomOption === 'selectFrameColor')
      this._selectFrameColor = value;
    if(nomOption === 'dragCode')
      this._dragCode = value;
    if(nomOption === 'dropCode')
      this._dropCode = value;
    if(nomOption === 'dragData')
      this._dragData = value;
    if(nomOption === 'selected')
      this._selectSprite.visible = value;
    if(nomOption === 'selectOpacity')
      this._selectSprite.opacity = Math.min(Math.max(0,Number(value)),255);
    if(nomOption === 'popupText')
      this._popupText = value;
  }
  this.refresh();
};

/**********************************************************************
* Méthode : setState
* Fonction : Permet de changer l'etat du gui
* Params : pNumState : numero de l'état
**********************************************************************/
Gui_Base.prototype.setState = function(pNumState) {
  var save = this._imageState;
  this._imageState = Math.min(pNumState, this._imageStateMax -1);
  if(save !== this._imageState)
    this.refresh();
};

/**********************************************************************
* Méthode : checkLoadImage
* Fonction : dans le cas de l'utilisation d'un fichier image, cette fonction
*             permet de vérifier la fin du chargement de l'image
* Params : --
**********************************************************************/
Gui_Base.prototype.checkLoadImage = function(){
  if(this._needWaitLoad && this._imageUrl !== '' && this._bitmapUrl){
    if(this._bitmapUrl.isReady()){
      this._needCreateBitmap = true;
      this._needWaitLoad = false;
      this.refresh();
    }
  }
};

/**********************************************************************
* Méthode : update (surcharge)
* Fonction : Permet d'appeler la vérification de fin de chargement d'image
* Params : --
**********************************************************************/
Gui_Base.prototype.update = function(){
  Sprite.prototype.update.call(this);
  this.checkLoadImage();
  if(this._focus && (Input.isTriggered('enter') || (this._validOnSpace && Input.isTriggered('space'))))
    this.onValid();
  if(this._selectSprite.visible){
    this._selectSprite.opacity += this._selectSensOpacity;
    if(this._selectSprite.opacity < 50)
      this._selectSensOpacity = 3;
    if(this._selectSprite.opacity > 100)
      this._selectSensOpacity = -3;
  }
  
};

/**********************************************************************
* Méthode : activate
* Fonction : Permet d'activer le gui
* Params : --
**********************************************************************/
Gui_Base.prototype.activate = function() {
  this._active = true;
};

/**********************************************************************
* Méthode : deactivate
* Fonction : Permet de desactiver le gui
* Params : --
**********************************************************************/
Gui_Base.prototype.deactivate = function() {
  this._active = false;
};

/**********************************************************************
* Méthode : updateMousePos
* Fonction : Permet de mettre à jour le statut du Gui
* Params : x, y, coordonnées souris relatif au contents de la fenêtre
**********************************************************************/
Gui_Base.prototype.updateMousePos = function(x,y) {
  if(this._active){
    var rollover = (x >= this.x && x <= (this.x + this.width) && y >= this.y && y <= (this.y + this.height));
    if(this._rollover && !rollover){
      this.onRollout();
      if(this._cptFrameBeforePopup === this._nbrFrameForPopup){
        SceneManager.showPopup(this, false);
        this.onPopupHide();
      }
      this._cptFrameBeforePopup = 0;
    }
    this._rollover = rollover;
    if(this._rollover){
      this.setState(1);
      this.onRollover();
      
      if(this._cptFrameBeforePopup !== this._nbrFrameForPopup)
      {
        this._cptFrameBeforePopup ++;
        if(this._cptFrameBeforePopup === this._nbrFrameForPopup){
          SceneManager.showPopup(this, true);
          this.onPopupShow();
        }
      }
      
      if(TouchInput.isReleased()){
        if(this._click){
          this._click = false;
          this.onClick();
        }
        SceneManager.validDrop(this);

      }else if(TouchInput.isTriggered() && !this._click){
        this._click = true;
        if(!this._focus)
          this.setFocus(true);
      }
      if(this._click)
        this.setState(2);
    }else{
      if(!this._focus)
        this.setState(0);
      if(this._click){
        if(TouchInput.isReleased()){
          this._click = false;
          this.onClickout();
        }
        if(this._dragCode !== '')
          SceneManager.startDragDrop(this, this._dragData);
      }
    }
  }
};

/**********************************************************************
* Méthode : select
* Fonction : Permet de sélectionner le GUI
* Params : --
**********************************************************************/
Gui_Base.prototype.select = function() {
  this._selectSprite.visible = true;
  this.onSelect();
};

/**********************************************************************
* Méthode : select
* Fonction : Permet de sélectionner le GUI
* Params : --
**********************************************************************/
Gui_Base.prototype.deselect = function() {
  this._selectSprite.visible = false;
  this.onDeselect();
};
 
/**********************************************************************
* Méthode : isSelect
* Fonction : Permet de sélectionner le GUI
* Params : --
**********************************************************************/
Gui_Base.prototype.isSelect = function() {
  return this._selectSprite.visible;
};

/**********************************************************************
* Méthode : refresh
* Fonction : Permet de recréer l'affichage du Gui
* Params : --
**********************************************************************/
Gui_Base.prototype.refresh = function() {
  if(!this._needWaitLoad){
    if(this._needCreateBitmap){
      this.bitmap = new Bitmap(this.width, this.height);
      this._needCreateBitmap = false;
    }else
      this.bitmap.clear();
    
    this.drawBackColor();
    this.drawImageLoad();
    this.drawText();
    this.drawFrame();
    this.drawSelect();
  }
};

/**********************************************************************
* Méthode : drawImageLoad
* Fonction : Permet de réaliser un transfert de l'image chargé vers le Gui
* Params : --
**********************************************************************/
Gui_Base.prototype.drawImageLoad = function() {
  if(this._imageUrl !== ''){
    if(this._rectBitmap === null)
      if(this._offsetImgFragment === 0)
        this.bitmap.blt(this._bitmapUrl, 0, (this._bitmapUrl.height / this._imageStateMax)*this._imageState, this._bitmapUrl.width, this._bitmapUrl.height / this._imageStateMax, 0, 0, this.width, this.height);
      else{
        // transférer 9 fragments de l'image
        this.bitmap.blt(this._bitmapUrl, 0, (this._bitmapUrl.height / this._imageStateMax)*this._imageState, this._offsetImgFragment, this._offsetImgFragment, 0, 0, this._offsetImgFragment, this._offsetImgFragment);
        this.bitmap.blt(this._bitmapUrl, this._offsetImgFragment, (this._bitmapUrl.height / this._imageStateMax)*this._imageState, this._bitmapUrl.width - (this._offsetImgFragment * 2), this._offsetImgFragment, this._offsetImgFragment, 0, this.width - (this._offsetImgFragment * 2), this._offsetImgFragment);
        this.bitmap.blt(this._bitmapUrl, this._bitmapUrl.width - this._offsetImgFragment, (this._bitmapUrl.height / this._imageStateMax)*this._imageState, this._offsetImgFragment, this._offsetImgFragment, this.width - this._offsetImgFragment, 0, this._offsetImgFragment, this._offsetImgFragment);
        
        this.bitmap.blt(this._bitmapUrl, 0, (this._bitmapUrl.height / this._imageStateMax)*this._imageState + this._offsetImgFragment, this._offsetImgFragment, (this._bitmapUrl.height / this._imageStateMax) - (this._offsetImgFragment * 2), 0, this._offsetImgFragment, this._offsetImgFragment, this.height - (this._offsetImgFragment * 2));
        this.bitmap.blt(this._bitmapUrl, this._offsetImgFragment, (this._bitmapUrl.height / this._imageStateMax)*this._imageState + this._offsetImgFragment, this._bitmapUrl.width - (this._offsetImgFragment * 2), (this._bitmapUrl.height / this._imageStateMax) - (this._offsetImgFragment * 2), this._offsetImgFragment, this._offsetImgFragment, this.width - (this._offsetImgFragment * 2), this.height - (this._offsetImgFragment * 2));
        this.bitmap.blt(this._bitmapUrl, this._bitmapUrl.width - this._offsetImgFragment, (this._bitmapUrl.height / this._imageStateMax)*this._imageState + this._offsetImgFragment, this._offsetImgFragment, (this._bitmapUrl.height / this._imageStateMax) - (this._offsetImgFragment * 2), this.width - this._offsetImgFragment, this._offsetImgFragment, this._offsetImgFragment, this.height - (this._offsetImgFragment * 2));
        
        this.bitmap.blt(this._bitmapUrl, 0, (this._bitmapUrl.height / this._imageStateMax)*(this._imageState + 1) - this._offsetImgFragment, this._offsetImgFragment, this._offsetImgFragment, 0, this.height - this._offsetImgFragment, this._offsetImgFragment, this._offsetImgFragment);
        this.bitmap.blt(this._bitmapUrl, this._offsetImgFragment, (this._bitmapUrl.height / this._imageStateMax)*(this._imageState + 1) - this._offsetImgFragment, this._bitmapUrl.width - (this._offsetImgFragment * 2), this._offsetImgFragment, this._offsetImgFragment, this.height - this._offsetImgFragment, this.width - (this._offsetImgFragment * 2), this._offsetImgFragment);
        this.bitmap.blt(this._bitmapUrl, this._bitmapUrl.width - this._offsetImgFragment, (this._bitmapUrl.height / this._imageStateMax)*(this._imageState + 1) - this._offsetImgFragment, this._offsetImgFragment, this._offsetImgFragment, this.width - this._offsetImgFragment, this.height - this._offsetImgFragment, this._offsetImgFragment, this._offsetImgFragment);
      }
        
    else
      this.bitmap.blt(this._bitmapUrl, this._rectBitmap.x, this._rectBitmap.y, this._rectBitmap.width, this._rectBitmap.height, 0, 0, this.width, this.height);
  }
  
};

/**********************************************************************
* Méthode : drawBackColor
* Fonction : Permet de remplir le gui d'une couleur de fond
* Params : --
**********************************************************************/
Gui_Base.prototype.drawBackColor = function() {
  if(this._backColor !== '')
    this.bitmap.fillRect(0 , 0, this.width, this.height, this._backColor);
};

/**********************************************************************
* Méthode : drawText
* Fonction : Permet de dessiner le texte
* Params : --
**********************************************************************/
Gui_Base.prototype.drawText = function() {
  if(this._text !== ''){
    this.bitmap.textColor = this._textColor;
    this.bitmap.fontFace = this._fontFace;
    this.bitmap.fontSize = this._fontSize;
    this.bitmap.fontItalic = this._fontItalic;
    this.bitmap.drawText(this._text, this._padding, this._padding, this.width - (this._padding * 2), this.height - (this._padding * 2), this._textAlign);
  }
};

/**********************************************************************
* Méthode : drawFrame
* Fonction : Permet de dessiner le cadre
* Params : --
**********************************************************************/
Gui_Base.prototype.drawFrame = function() {
  if(this._frameColor !== ''){
    this.bitmap.drawFrame(0, 0, this.width, this.height, this._frameColor, 1);
  }
};

/**********************************************************************
* Méthode : drawSelect
* Fonction : Permet de dessiner le cadre
* Params : --
**********************************************************************/
Gui_Base.prototype.drawSelect = function() {
  this._selectSprite.bitmap = new Bitmap(this.width + 4, this.height + 4);
  this._selectSprite.bitmap.fillRect(0, 0, this.width + 4, this.height + 4, this._selectColor);
  this._selectSprite.bitmap.drawFrame(0, 0, this.width + 4, this.height + 4, this._selectFrameColor, 2);
};

/**********************************************************************
* Méthode : setFocus
* Fonction : Permet de postionner l'état de focus du Gui
* Params : pFocus : vrai ou faux
**********************************************************************/
Gui_Base.prototype.setFocus = function(pFocus) {
  if(this._focusable){
    if(!this._focus && pFocus){
      this.setState(1);
      this.onFocus();
    }
    if(this._focus && !pFocus){
      this.setState(0);
      this.onFocusOut();
    }
    this._focus = pFocus;
  }
};

/**********************************************************************
* Méthode : setHandler
* Fonction : Ajoute un lien entre un événement et une fonction
* Params : symbol : nom de l'événement (onrollover, onfocus, etc...)
*          method : définition de la fonction a appler
**********************************************************************/
Gui_Base.prototype.setHandler = function(symbol, method) {
  this._handlers[symbol] = method;
};

/**********************************************************************
* Méthode : isHandled
* Fonction : Vérifi l'existance d'un lien entre un event et une fonction
* Params : symbol : nom de l'event
**********************************************************************/
Gui_Base.prototype.isHandled = function(symbol) {
  return !!this._handlers[symbol];
};

/**********************************************************************
* Méthode : callHandler
* Fonction : Appel la fonction attachée à l'event
* Params : --
**********************************************************************/
Gui_Base.prototype.callHandler = function(symbol) {
  if (this.isHandled(symbol)) {
    this._handlers[symbol]();
  }
};
Gui_Base.prototype.onRollover = function(){
  this.callHandler('onrollover');
};
Gui_Base.prototype.onRollout = function(){
  this.callHandler('onrollout');
};
Gui_Base.prototype.onFocus = function(){
  this.callHandler('onfocus');
};
Gui_Base.prototype.onFocusOut = function(){
  this.callHandler('onfocusout');
};
Gui_Base.prototype.onClick = function(){
  this.callHandler('onclick');
};
Gui_Base.prototype.onClickout = function(){
  this.callHandler('onclickout');
};
Gui_Base.prototype.onValid = function(){
  this.callHandler('onvalid');
};
Gui_Base.prototype.onSelect = function(){
  this.callHandler('onselect');
};
Gui_Base.prototype.onDeselect = function(){
  this.callHandler('ondeselect');
};
Gui_Base.prototype.onDrag = function(){
  this.callHandler('ondrag');
};
Gui_Base.prototype.onDrop = function(){
  this.callHandler('ondrop');
};
Gui_Base.prototype.onPopupShow = function(){
  this.callHandler('onpopupshow');
};
Gui_Base.prototype.onPopupHide = function(){
  this.callHandler('onpopuphide');
};



/**********************************************************************
*----------------------------------------------------------------------
* Création d'une nouvelle "classe" Gui_Url
*  hérite de la classe Gui_Base
*----------------------------------------------------------------------
**********************************************************************/
function Gui_Url() {
  this.initialize.apply(this, arguments);
}

Gui_Url.prototype = Object.create(Gui_Base.prototype);
Gui_Url.prototype.constructor = Gui_Url;

/**********************************************************************
* Méthode : initialize
* Fonction : initialisation des propriétés du Gui_Url
* Params : --
**********************************************************************/
Gui_Url.prototype.initialize = function(pOptions) {
  Gui_Base.prototype.initialize.call(this);
  this._urlWeb = '';
  this._urlColor = '#4444ff';
  this._urlRollover = false;
  this._focusable = true;
  this.setOptions(pOptions);
};

/**********************************************************************
* Méthode : setOptions (surcharge)
* Fonction : modification des options
* Params : pOptions : JSON des options à modifier
**********************************************************************/
var _Gui_Url_setOptions = Gui_Base.prototype.setOptions;
Gui_Url.prototype.setOptions = function(pOptions) {
  for(var nomOption in pOptions){
    var value = pOptions[nomOption];
    if(nomOption === 'url'){
      this._urlWeb = value;
      this._text = value;
    }
    if(nomOption === 'urlColor')
      this._urlColor = value;
  }
  _Gui_Url_setOptions.call(this, pOptions);
};

/**********************************************************************
* Méthode : onRollover (surcharge)
* Fonction : permet de conserver l'état rollover et de lancer un refresh
* Params : --
**********************************************************************/
var _Gui_Url_onRollover = Gui_Base.prototype.onRollover;
Gui_Url.prototype.onRollover = function(){
  _Gui_Url_onRollover.call(this);
  if(!this._urlRollover){
    this._urlRollover = true;
    this.refresh();
  }
};

/**********************************************************************
* Méthode : onRollout (surcharge)
* Fonction : permet de retirer l'état rollover et de lancer un refresh
* Params : --
**********************************************************************/
Gui_Url.prototype.onRollout = function(){
  Gui_Base.prototype.onRollout.call(this);
  this._urlRollover = false;
  this.refresh();
};

/**********************************************************************
* Méthode : onClick (surcharge)
* Fonction : permet d'ouvrir une nouvelle page web
* Params : --
**********************************************************************/
Gui_Url.prototype.onClick = function(){
  Gui_Base.prototype.onClick.call(this);
  this.openUrl();
};

/**********************************************************************
* Méthode : onValid (surcharge)
* Fonction : permet d'ouvrir une nouvelle page web
* Params : --
**********************************************************************/
Gui_Url.prototype.onValid = function(){
  Gui_Base.prototype.onValid.call(this);
  this.openUrl();
};

/**********************************************************************
* Méthode : openUrl
* Fonction : permet d'ouvrir une nouvelle page web
* Params : --
**********************************************************************/
Gui_Url.prototype.openUrl = function(){
  if(this._urlWeb !== ''){
    var win = window.open(this._urlWeb, '_blank');
    if(win)
      win.focus();
  }
};

/**********************************************************************
* Méthode : drawText
* Fonction : Permet de dessiner le lien surligné
* Params : --
**********************************************************************/
Gui_Url.prototype.drawText = function() {
  if(this._text !== ''){
    if(this._urlRollover)
      this.bitmap.textColor = this._urlColor;
    else
      this.bitmap.textColor = this._textColor;

    this.bitmap.fontFace = this._fontFace;
    this.bitmap.fontSize = this._fontSize;
    this.bitmap.fontItalic = this._fontItalic;
    var lngText = Math.min(this.bitmap.measureTextWidth(this._text),this.width - (this._padding * 2));
    var deb = 0;
    if(this._textAlign === 'center')
      deb = (this.width - (this._padding * 2) - lngText)/2;
    if(this._textAlign === 'right')
      deb = (this.width - (this._padding * 2) - lngText);
    var ty = Math.round((this.height - (this.height - this._fontSize * 0.7) / 2) + 4);
    this.bitmap.fillRect(deb, ty, lngText, 2, this.bitmap.textColor);
    this.bitmap.drawText(this._text, this._padding, this._padding, this.width - (this._padding * 2), this.height - (this._padding * 2), this._textAlign);
  }
};


/**********************************************************************
*----------------------------------------------------------------------
* Création d'une nouvelle "classe" Gui_Button
*  hérite de la classe Gui_Base
*----------------------------------------------------------------------
**********************************************************************/
function Gui_Button() {
  this.initialize.apply(this, arguments);
}

Gui_Button.prototype = Object.create(Gui_Base.prototype);
Gui_Button.prototype.constructor = Gui_Button;

/**********************************************************************
* Méthode : initialize
* Fonction : initialisation des propriétés du Gui_Button
* Params : --
**********************************************************************/
Gui_Button.prototype.initialize = function(pOptions) {
  Gui_Base.prototype.initialize.call(this);
  this._imageStateMax = 3;
  this._focusable = true;
  this.setOptions(pOptions);
};

/**********************************************************************
* Méthode : onValid (surcharge)
* Fonction : permet de router vers onclick
* Params : --
**********************************************************************/
Gui_Button.prototype.onValid = function(){
  Gui_Base.prototype.onValid.call(this);
  this.onClick();
};


/**********************************************************************
*----------------------------------------------------------------------
* Création d'une nouvelle "classe" Gui_CheckBox
*  hérite de la classe Gui_Base
*----------------------------------------------------------------------
**********************************************************************/
function Gui_CheckBox() {
  this.initialize.apply(this, arguments);
}

Gui_CheckBox.prototype = Object.create(Gui_Base.prototype);
Gui_CheckBox.prototype.constructor = Gui_CheckBox;

/**********************************************************************
* Méthode : initialize
* Fonction : initialisation des propriétés du Gui_Button
* Params : --
**********************************************************************/
Gui_CheckBox.prototype.initialize = function(pOptions) {
  Gui_Base.prototype.initialize.call(this);
  this._check = false;
  this._checkColor = '#222222';
  this._frameCheckColor = '#000000';
  this._focusable = true;
  this.setOptions(pOptions);
};

/**********************************************************************
* Méthode : setOptions (surcharge)
* Fonction : modification des options
* Params : pOptions : JSON des options à modifier
**********************************************************************/
Gui_CheckBox.prototype.setOptions = function(pOptions) {
  for(var nomOption in pOptions){
    var value = pOptions[nomOption];
    if(nomOption === 'check'){
      this._check = value;
    }
    if(nomOption === 'checkColor'){
      this._checkColor = value;
    }
    if(nomOption === 'frameCheckColor'){
      this._frameCheckColor = value;
    }
  }
  Gui_Base.prototype.setOptions.call(this, pOptions);
};

/**********************************************************************
* Méthode : onClick (surcharge)
* Fonction : permet d'ouvrir une nouvelle page web
* Params : --
**********************************************************************/
Gui_CheckBox.prototype.onClick = function(){
  Gui_Base.prototype.onClick.call(this);
  this._check = !this._check;
  this.refresh();
  if(this._check)
    this.onCheck();
  else
    this.onUnCheck();
};


/**********************************************************************
* Méthode : onValid (surcharge)
* Fonction : permet de router vers onclick
* Params : --
**********************************************************************/
Gui_CheckBox.prototype.onValid = function(){
  Gui_Base.prototype.onValid.call(this);
  this.onClick();
};

/**********************************************************************
* Méthode : drawText
* Fonction : Permet de dessiner le lien surligné
* Params : --
**********************************************************************/
Gui_CheckBox.prototype.drawText = function() {
  var minValue = Math.min(this.width, this.height)-(this._padding*2);
  this.bitmap.drawFrame(this._padding, this._padding, minValue, minValue, this._frameCheckColor, 1);
  
  if(this._check){
    this.bitmap.drawLine(this._padding + 3, this._padding + 3, this._padding + minValue - 3, this._padding + minValue - 3, this._checkColor , 3);
    this.bitmap.drawLine(this._padding + 3, this._padding + minValue - 3, this._padding + minValue - 3, this._padding + 3, this._checkColor , 3);
  }
  
  if(this._text !== ''){
    this.bitmap.fontFace = this._fontFace;
    this.bitmap.fontSize = this._fontSize;
    this.bitmap.fontItalic = this._fontItalic;
    this.bitmap.drawText(this._text, minValue + (this._padding * 2), this._padding, this.width - (this._padding + minValue-(this._padding)), this.height - (this._padding * 2), this._textAlign);
  }
};

/**********************************************************************
* Méthode : onCheck, onuncheck
* Fonction : permet d'appeler les fonctions liées sur les events
* Params : --
**********************************************************************/
Gui_CheckBox.prototype.onCheck = function(){
  this.callHandler('oncheck');
};
Gui_CheckBox.prototype.onUnCheck = function(){
  this.callHandler('onuncheck');
};


/**********************************************************************
*----------------------------------------------------------------------
* Création d'une nouvelle "classe" Gui_Cursor
*  hérite de la classe Gui_Base
*----------------------------------------------------------------------
**********************************************************************/
function Gui_Cursor() {
  this.initialize.apply(this, arguments);
}

Gui_Cursor.prototype = Object.create(Gui_Base.prototype);
Gui_Cursor.prototype.constructor = Gui_Cursor;

/**********************************************************************
* Méthode : initialize
* Fonction : initialisation des propriétés du Gui_Cursor
* Params : --
**********************************************************************/
Gui_Cursor.prototype.initialize = function(pOptions) {
  Gui_Base.prototype.initialize.call(this);
  this._valueMin = 0;
  this._valueMax = 100;
  this._value = 0;
  this._cursorWidth = 20;
  this._cursorUrl = '';
  this._backgroundCursorColor = '';
  this._frameCursorColor = '';
  this._spriteCursor = new Sprite();
  this._spriteCursor.x = 1;
  this._spriteCursor.y = 1;
  this._offsetX = -1;
  this._needWaitLoadCursor = false;
  this._needCreateBitmapCursor = true;
  this._focusable = true;
  this.addChild(this._spriteCursor);
  this.setOptions(pOptions);
};

/**********************************************************************
* Méthode : setOptions (surcharge)
* Fonction : modification des options
* Params : pOptions : JSON des options à modifier
**********************************************************************/
Gui_Cursor.prototype.setOptions = function(pOptions) {
  for(var nomOption in pOptions){
    var value = pOptions[nomOption];
    if(nomOption === 'width')
      this.width = value;
    if(nomOption === 'valueMin')
      this._valueMin = value;
    if(nomOption === 'valueMax')
      this._valueMax = value;
    if(nomOption === 'value'){
      this._value = Math.max(Math.min(this._valueMax, value), this._valueMin);
      this._spriteCursor.x = 1 + (((this.width - this._cursorWidth - 2) * this._value)) / (this._valueMax - this._valueMin); 
    }
    if(nomOption === 'backgroundCursorColor')
      this._backgroundCursorColor = value;
    if(nomOption === 'frameCursorColor')
      this._frameCursorColor = value;
    if(nomOption === 'cursorUrl'){
      if(this._cursorUrl !== value){
        this._bitmapCursorUrl = Bitmap.load(value);
        this._needWaitLoadCursor = true;
        this._needCreateBitmapCursor = true;
      }
      this._cursorUrl = value;
    }
    if(nomOption === 'cursorWidth'){
      this._cursorWidth = value;
      this._needCreateBitmapCursor = true;
    }
  }
  Gui_Base.prototype.setOptions.call(this, pOptions);
};

/**********************************************************************
* Méthode : update (surcharge)
* Fonction : Permet d'appeler la vérification de fin de chargement d'image
* Params : --
**********************************************************************/
Gui_Cursor.prototype.update = function(){
  Gui_Base.prototype.update.call(this);
  this.checkLoadImageCursor();
};

/**********************************************************************
* Méthode : checkLoadImageCursor
* Fonction : Permet d'appeler la vérification de fin de chargement d'image
* Params : --
**********************************************************************/
Gui_Cursor.prototype.checkLoadImageCursor = function(){
  if(this._needWaitLoadCursor && this._cursorUrl !== '' && this._bitmapCursorUrl){
    if(this._bitmapCursorUrl.isReady()){
      this._needCreateBitmapCursor = true;
      this._needWaitLoadCursor = false;
      this.refresh();
    }
  }
};

/**********************************************************************
* Méthode : refresh
* Fonction : Permet de recréer l'affichage du Gui
* Params : --
**********************************************************************/
Gui_Cursor.prototype.refresh = function() {
  Gui_Base.prototype.refresh.call(this);
  if(!this._needWaitLoadCursor){
    if(this._needCreateBitmapCursor){
      this._spriteCursor.bitmap = new Bitmap(this._cursorWidth, this.height-2);
      this._needCreateBitmapCursor = false;
    }else
      this._spriteCursor.bitmap.clear();

    if(this._backgroundCursorColor !== '')
      this._spriteCursor.bitmap.fillRect(0 , 0, this._spriteCursor.bitmap.width, this._spriteCursor.bitmap.height, this._backgroundCursorColor);
    if(this._cursorUrl !== '')
      this._spriteCursor.bitmap.blt(this._bitmapCursorUrl, 0, 0, this._spriteCursor.bitmap.width, this._spriteCursor.bitmap.height, 0, 0);
    if(this._frameCursorColor !== '')
      this._spriteCursor.bitmap.drawFrame(0, 0, this._spriteCursor.bitmap.width, this._spriteCursor.bitmap.height, this._frameCursorColor, 1);
      
  }
  this._value = Math.round(this._valueMin + (((this._valueMax - this._valueMin) * (this._spriteCursor.x - 1)) / (this.width - this._cursorWidth - 2)));
};


/**********************************************************************
* Méthode : updateMousePos
* Fonction : Permet de mettre à jour le statut du Gui
* Params : x, y, coordonnées souris relatif au contents de la fenêtre
**********************************************************************/
Gui_Cursor.prototype.updateMousePos = function(x,y) {
  var click = this._click;
  Gui_Base.prototype.updateMousePos.call(this, x, y);
  if(this._click){
    if(!click){
      if(x >= this.x + this._spriteCursor.x && x <= this.x + this._spriteCursor.x + this._cursorWidth - 1)
        this._offsetX = x - (this.x + this._spriteCursor.x);
      else{
        this._offsetX = Math.round(this._cursorWidth / 2);
      }
    }
    this._spriteCursor.x = Math.min(Math.max(1, x - this.x - this._offsetX), this.x + this.width - this._cursorWidth - 1) ;
    this._value = Math.round(this._valueMin + (((this._valueMax - this._valueMin) * (this._spriteCursor.x - 1)) / (this.width - this._cursorWidth - 2)));
    this.onChangeValue();
  }
};


Gui_Cursor.prototype.onChangeValue = function(){
  this.callHandler('onchangevalue');
};

/**********************************************************************
* Méthode : getValue
* Fonction : Retourne la valeur en fonction des max et min et la position du curseur
* Params : --
**********************************************************************/
Gui_Cursor.prototype.getValue = function() {
  return this._value;
};


/**********************************************************************
*----------------------------------------------------------------------
* Création d'une nouvelle "classe" Gui_TextInput
*  hérite de la classe Gui_Base
*----------------------------------------------------------------------
**********************************************************************/
function Gui_TextInput() {
  this.initialize.apply(this, arguments);
}

Gui_TextInput.prototype = Object.create(Gui_Base.prototype);
Gui_TextInput.prototype.constructor = Gui_TextInput;

/**********************************************************************
* Méthode : initialize
* Fonction : initialisation des propriétés du Gui_TextInput
* Params : --
**********************************************************************/
Gui_TextInput.prototype.initialize = function(pOptions) {
  Gui_Base.prototype.initialize.call(this);
  this._posCursor = 0;
  this._posCursorPxl = 0;
  this._offsetCursor = 0;
  this._spriteCursor = new Sprite();
  this._spriteCursor.visible = false;
  this._spriteCursor.bitmap = new Bitmap(1,1);
  this._textBitmap = new Bitmap(1,1);
  //this._frameColor = '#000000';
  this._padding = 4;
  this._maxChar = 0;
  this._needRefreshCursor = true;
  this._focusable = true;
  this._validOnSpace = false;
  this._password = false;
  this._cursorNbFrame = 30;
  this._cursorFrame = 0;
  this.addChild(this._spriteCursor);
  this.setOptions(pOptions);

};

/**********************************************************************
* Méthode : setOptions (surcharge)
* Fonction : modification des options
* Params : pOptions : JSON des options à modifier
**********************************************************************/
Gui_TextInput.prototype.setOptions = function(pOptions) {
  for(var nomOption in pOptions){
    var value = pOptions[nomOption];
    if(nomOption === 'heigth'){
      this._needRefreshCursor = true;
      this.heigth = value;
    }
    if(nomOption === 'padding'){
      this._needRefreshCursor = true;
      this._padding = value;
    }
    if(nomOption === 'maxChar')
      this._maxChar = value;
    if(nomOption === 'password')
      this._password = value;
    if(nomOption === 'text'){
      this._posCursor = value.length;
      this._needRefreshCursor = true;
    }
  }
  Gui_Base.prototype.setOptions.call(this, pOptions);
};

/**********************************************************************
* Méthode : textInGui
* Fonction : retourne le texte affiché dans le gui
* Params : --
**********************************************************************/
Gui_TextInput.prototype.textInGui = function() {
  if(!this._password)
    return this._text;
  var text = '';
  for(var i = 0; i < this._text.length; i ++)
    text += '*';
  return text;
};

/**********************************************************************
* Méthode : refresh
* Fonction : Permet de recréer l'affichage du Gui
* Params : --
**********************************************************************/
Gui_TextInput.prototype.refresh = function() {
  if(this._needRefreshCursor){
    this._spriteCursor.bitmap.resize(1, this.height - ((this._padding + 2) * 2));
    this._spriteCursor.height = this.height - ((this._padding + 2) * 2);
    this._spriteCursor.bitmap.fillRect(0, 0, 1, this.height - ((this._padding + 2) * 2), this._textColor);
    this._spriteCursor.y = 2 + this._padding;
    this._needRefreshCursor = false;
  }
  Gui_Base.prototype.refresh.call(this);
};


/**********************************************************************
* Méthode : drawText
* Fonction : Permet de dessiner le texte
* Params : --
**********************************************************************/
Gui_TextInput.prototype.drawText = function() {
  this._textBitmap.textColor = this._textColor;
  this._textBitmap.fontFace = this._fontFace;
  this._textBitmap.fontSize = this._fontSize;
  this._textBitmap.fontItalic = this._fontItalic;
  
  this.positionCursor();
  
  if(this._text !== ''){
    var textSize = this._textBitmap.measureTextWidth(this.textInGui()) + 10;
    this._textBitmap.resize(textSize , this.height - (this._padding*2));
    this._textBitmap.drawText(this.textInGui(), 2, 0, this._textBitmap.width, this._textBitmap.height);
    var bltWidth = Math.min(this._textBitmap.width - this._offsetCursor, this.width - (this._padding * 2));
    this.bitmap.blt(this._textBitmap, this._offsetCursor, 0, bltWidth, this._textBitmap.height, this._padding, this._padding, bltWidth, this._textBitmap.height);
  }
};

/**********************************************************************
* Méthode : refresh
* Fonction : Permet de recréer l'affichage du Gui
* Params : --
**********************************************************************/
Gui_TextInput.prototype.positionCursor = function() {
  var needRefresh = false;
  var testFragment = this.textInGui().slice(0, this._posCursor);
  this._posCursorPxl = this._textBitmap.measureTextWidth(testFragment);
  
  if(2 + this._padding + this._posCursorPxl - this._offsetCursor > this.width - (this._padding + 2)){
    this._offsetCursor = (2 + this._padding + this._posCursorPxl) - (this.width - (this._padding + 2));
    needRefresh = true;
  }
  
  if(2 + this._padding + this._posCursorPxl - this._offsetCursor < this._padding + 2){
    this._offsetCursor = this._posCursorPxl;
    needRefresh = true;
  }
  
  this._spriteCursor.x = 2 + this._padding + this._posCursorPxl - this._offsetCursor;
  this._cursorFrame = 0;

  return needRefresh;
};

/**********************************************************************
* Méthode : update (surcharge)
* Fonction : Permet d'appeler la vérification de fin de chargement d'image
* Params : --
**********************************************************************/
Gui_TextInput.prototype.update = function(){
  Gui_Base.prototype.update.call(this);
  if(this._active && this._focus){
    var lettre = Input.getLetter();
    if(lettre !== ''){
      this._text = this._text.slice(0, this._posCursor) + lettre + this._text.slice(this._posCursor);
      this._posCursor += lettre.length;
      this.refresh();
      this.onChangeValue();
    }
    if(Input.isRepeated('backspace') && this._posCursor > 0){
      this._text = this._text.slice(0, Math.max(this._posCursor - 1)) + this._text.slice(this._posCursor);
      this._posCursor --;
      this.refresh();
      this.onChangeValue();
    }
    if(Input.isRepeated('delete')  && this._posCursor < this._text.length){
      this._text = this._text.slice(0, Math.max(this._posCursor)) + this._text.slice(this._posCursor + 1);
      this.refresh();
      this.onChangeValue();
    }
    if(Input.isRepeated('left') && this._posCursor > 0){
      this._posCursor --;
      if(this.positionCursor())
        this.refresh();
    }
    if(Input.isRepeated('right') && this._posCursor < this._text.length){
      this._posCursor ++;
      if(this.positionCursor())
        this.refresh();
    }
    if(Input.isRepeated('home') && this._posCursor > 0){
      this._posCursor = 0;
      if(this.positionCursor())
        this.refresh();
    }
    if(Input.isRepeated('end') && this._posCursor < this._text.length){
      this._posCursor = this._text.length;
      if(this.positionCursor())
        this.refresh();
    }

    this._cursorFrame ++;
    if(this._cursorFrame > this._cursorNbFrame)
      this._spriteCursor.visible = false;
    else
      this._spriteCursor.visible = true;
    if(this._cursorFrame > this._cursorNbFrame * 2)
      this._cursorFrame = 0;
  }
  else
    this._spriteCursor.visible = false;
};


Gui_TextInput.prototype.onFocus = function(){
  Gui_Base.prototype.onFocus.call(this);
  this._cursorFrame = 0;
};

Gui_TextInput.prototype.onChangeValue = function(){
  this.callHandler('onchangevalue');
};
